#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @File       :  test_login.py [Python]
# @Date       :  2021/12/15
# @Author     :  mitool
# @Email      :  
# @Software   :  PyCharm
# @Version    :  1.0
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------

import pytest

from webAuto.Gitee.page.homePage import Home
from webAuto.Gitee.page.loginPage import Login

class TestLogin(object):

    @pytest.mark.parametrize(['user', 'pwd'], [
        ("253447917@qq.com", "mitool@19891023"),
    ])
    def test_login(self, cst_driver, user, pwd):
        home = Home(cst_driver)
        home.login()
        login = Login(cst_driver)
        login.user(keywords=user)
        login.password(keywords=pwd)
        login.login()