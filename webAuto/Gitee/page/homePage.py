#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @File       :  homePage.py [Python]
# @Date       :  2021/12/14
# @Author     :  mitool
# @Email      :  
# @Software   :  PyCharm
# @Version    :  1.0
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------

from webAuto.Gitee import filter_loc


class Home(object):

    def __init__(self, driver):
        self.driver = driver

    @filter_loc
    def login(self, loc: tuple):
        self.driver.click(loc)

    @filter_loc
    def register(self, loc: tuple):
        self.driver.click(loc)

    def search(self, loc: tuple, keywords):
        self.driver.send_keys(loc, keywords)
