#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @File       :  settings.py [Python]
# @Date       :  2021/11/21
# @Author     :  mitool
# @Email      :  
# @Software   :  PyCharm
# @Version    :  1.0
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------

import os
import platform
import time
from loguru import logger

PY_VERSION                      = int(platform.python_version_tuple()[0])
DATE                            = time.strftime("%Y_%m_%d")

'''path settings'''
ROOT_PATH                       = os.path.dirname(os.path.realpath(__file__))
CONF_PATH                       = os.path.join(ROOT_PATH, "config")
DRIVER_PATH                     = os.path.join(ROOT_PATH, "packages")
LOG_PATH                        = os.path.join(ROOT_PATH, "logs")

logger.add(f"{LOG_PATH}/info_{DATE}.log",
           rotation="500MB",
           encoding="utf-8",
           enqueue=True,
           retention="10 days",
           level="INFO")

