#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2017/11/15 17:39
# @Author  : MITool
# @File    : iniHelper.py
# @Version : 2.0
# @Desc    : 读取配置文件类,配置文件为filename.ini
# @Software: PyCharm


# ---------CODE BEGIN----------
import os
import codecs

from webAuto import settings

# python3 configparser, python2 ConfigParser
if settings.PY_VERSION == 3:
    import configparser
else:
    import ConfigParser as configparser



class ini(object):
    """
    读取ini配置文件模块
    """

    def __init__(self, path):
        """
        :param path: 配置文件的路径
        eg:
        cc = ini('../ProxyServer/ProxyConfig.ini')
        """
        cur_path = os.path.dirname(os.path.abspath(__file__))
        self.path = os.path.join(cur_path, path)
        if not os.path.exists(self.path):
            raise FileExistsError(self.path)
        if not os.path.isfile(self.path):
            raise TypeError(self.path)

        self.cf = configparser.ConfigParser()
        self.cf.read(self.path)

    def as_dict(self):
        '''
        :return: conf as dict.
        '''
        return {section: self.items(section) for section in self.sections()}

    def sections(self):
        """
        获取section列表
        :return:LIST
        """
        return self.cf.sections()

    def has_section(self, section):
        '''
        判断在section下是否存在
        :param section:
        :return:
        '''
        return self.cf.has_section(section)

    def options(self, section):
        """
        获取单个section下option的列表
        :param section:
        :return: LIST
        """
        if not self.has_section(section):
            raise configparser.NoSectionError(section)
        return self.cf.options(section)

    def items(self, section, vars={}):
        '''
        获取某个section的k-v值
        :param section:
        :param vars: update dict
        :return: dict
        '''
        return dict(self.cf.items(section, vars=vars))

    def has_option(self, section, option):
        """
        判断在section下是否存在option
        :param section:
        :param option:
        :return: bool
        """
        if self.cf.has_option(section, option):
            return True
        return False

    def get(self, section, option):
        try:
            # Ugly fix to avoid '0' and '1' to be parsed as a boolean value.
            # We raise an exception to goto fail, parse it as integer.
            if self.cf.get(section, option) in ["0", "1"]:
                raise ValueError
            value = self.cf.getboolean(section, option)
        except ValueError:
            try:
                value = self.cf.getint(section, option)
            except ValueError:
                value = self.cf.get(section, option)
        return value


    def set(self, section, option, value=None):
        """
        修改某个section下option的值为value,如果section不存则添加
        :param section:
        :param option:
        :param value:
        :return:
        """
        if not self.has_section(section):
            self.cf.add_section(section)
        self.cf.set(section, option, value)
        with codecs.open(self.path, 'w', encoding='utf-8') as fp:
            self.cf.write(fp)