#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @File       :  __init__.py.py [Python]
# @Date       :  2021/12/23
# @Author     :  mitool
# @Email      :  
# @Software   :  PyCharm
# @Version    :  1.0
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------

from webAuto.cores.parseSwagger.loader import *
from webAuto.cores.parseSwagger.swagger import Swagger


def parse(url, **kwargs):
    source = load_url(url, **kwargs)
    return Swagger(source)


def parse_file(path):
    source = load_file(path)
    return Swagger(source)
