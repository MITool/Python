#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @File       :  swagger.py [Python]
# @Date       :  2021/12/23
# @Author     :  mitool
# @Email      :  
# @Software   :  PyCharm
# @Version    :  1.0
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------

import re
import json
import urllib.parse
import uuid
import copy


class IncompleteModelError(BaseException):
    pass


class Swagger(object):

    def __init__(self, source):
        self.source = source

        self.__scheme = self.schemes[0]
        self.__host = self.source.get('host') or 'localhost'

    @property
    def schemes(self):
        return self.source.get('schemes') or ['http']

    @property
    def scheme(self):
        return self.__scheme

    @scheme.setter
    def scheme(self, value):
        self.__scheme = value

    @property
    def host(self):
        return self.__host

    @host.setter
    def host(self, value):
        self.__host = value

    @property
    def base_path(self):
        return self.source.get('basePath') or '/'

    @property
    def info(self):
        return self.source.get('info', None)

    @property
    def tags(self):
        return [tag.get('name') for tag in self.source.get('tags')]

    def __property__(self, prop):
        if not isinstance(prop, dict):
            raise TypeError
        _type = prop.get('type')
        _format = prop.get('format')
        _example = prop.get('example')
        if _example:
            prop = 'string'
        elif _type == 'integer':
            prop = 'int'
        elif _type == 'number':
            prop = 'float'
        elif _type == 'boolean':
            prop = 'bool'
        elif _type == 'string':
            if _format in [None, 'byte', 'binary', 'password']:
                prop = 'string'
            elif _format == 'date':
                prop = 'date'
            elif _format == 'date-time':
                prop = 'date-time'
        elif _type == 'file':
            prop = 'file'
        elif _type == 'array':
            prop = []

        return prop

    @property
    def models(self):
        ref_results = dict()
        # 保存没有内嵌对象的对象
        temp = list()

        definitions = copy.deepcopy(self.source.get('definitions'))

        # 提取没有内嵌对象的对象
        for key in list(definitions.keys()):
            properties = definitions[key].get('properties')
            if properties:
                ref_keys = re.findall(r'"#/definitions/(.+?)"', json.dumps(properties, ensure_ascii=False))
                if len(ref_keys) == 0:
                    temp.append(key)
            else:
                del definitions[key]
                ref_results.update({key: dict()})

        # 解析没有内嵌对象的对象，存放在ref_results
        for key in temp:
            properties = definitions[key].get('properties')
            if properties:
                for prop_name, prop in properties.items():
                    properties[prop_name] = self.__property__(prop)
                    ref_results.update({key: properties})
            del definitions[key]

        # 解析存在内嵌对象的对象，存放在ref_results
        while len(definitions.keys()) > 0:
            for key in list(definitions.keys()):
                properties = definitions[key].get('properties')
                if properties:
                    ref_keys = re.findall(r'"#/definitions/(.+?)"', json.dumps(properties, ensure_ascii=False))
                    if ref_keys and set(ref_keys).intersection(set(ref_results.keys())) == set(ref_keys):
                        for prop_name, prop in properties.items():
                            ref_key = re.search(r'"#/definitions/(.+?)"', json.dumps(prop, ensure_ascii=False))
                            if ref_key:
                                if properties.get(prop_name).get('type') == 'array':
                                    properties[prop_name] = [ref_results.get(ref_key.group(1))]
                                else:
                                    properties[prop_name] = ref_results.get(ref_key.group(1))
                            else:
                                properties[prop_name] = self.__property__(prop)
                        ref_results.update({key: properties})
                        del definitions[key]

        return ref_results

    @property
    def apis(self):
        models = self.models

        api_results = []

        for path, forms in self.source.get('paths').items():
            for method, form in forms.items():
                name = form.get('operationId')
                content_type = form.get('consumes') or ['application/x-www-form-urlencoded']
                content_type = content_type[0]
                tags = form.get('tags') or ['']
                summary = form.get('summary') or form.get('description') or None

                _path = (self.base_path + path).replace('//', '/')
                url = urllib.parse.urlunparse((self.scheme, self.host, _path, None, None, None))
                _api = {
                    'id': uuid.uuid4().hex,
                    'name': name,
                    'tags': tags[0],
                    'summary': summary,
                    'method': method,
                    'path': _path,
                    'url': url,
                    'headers': {'Content-Type': content_type},
                    'paths': dict(),
                    'query': dict(),
                    'json': dict(),
                    'form': dict(),
                    'formData': dict()
                }
                # 参数提取
                for param in form.get('parameters'):
                    _in = param.get('in')
                    name = param.get('name')
                    value = self.__property__(param)

                    if isinstance(value, dict):
                        schema = value.get('schema')
                        ref_key = re.search(r'"#/definitions/(.+?)"', json.dumps(schema, ensure_ascii=False))
                        if schema and ref_key:
                            if schema.get('type') == 'array':
                                value = [models.get(ref_key.group(1))]
                            else:
                                value = models.get(ref_key.group(1))
                        else:
                            value = []

                    if _in == 'header':
                        _api.get('headers').update({name: value})
                    elif _in == 'path':
                        _api.get('paths').update({name: value})
                    elif _in == 'query':
                        _api.get('query').update({name: value})
                    elif _in == 'body':
                        _api.update({'json': value})
                    elif _in == 'formData':
                        if 'multipart/form-data' in content_type:
                            _api.get('formData').update({name: value})
                        else:
                            _api.get('form').update({name: value})

                api_results.append(_api)
        return api_results
