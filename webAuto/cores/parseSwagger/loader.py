#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @File       :  loader.py [Python]
# @Date       :  2021/12/23
# @Author     :  mitool
# @Email      :  
# @Software   :  PyCharm
# @Version    :  1.0
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------

import json
import requests


def load_file(path):
    with open(path, encoding='utf8') as f:
        return json.loads(f.read())


def load_url(url, method='get', **kwargs):
    return requests.request(url=url, method=method, **kwargs).json()
