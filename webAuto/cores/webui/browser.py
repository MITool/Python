#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @File       :  browser.py [Python]
# @Date       :  2021/11/23
# @Author     :  mitool
# @Email      :  
# @Software   :  PyCharm
# @Version    :  1.0
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------
import time
from functools import wraps
from selenium.webdriver import ActionChains
from selenium.webdriver.support.ui import *
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException, TimeoutException

from webAuto.cores.webui.engine import Engine
from webAuto.settings import logger


def decorator(func):
    @wraps(func)
    def wrapper(self, loc: tuple, *args, **kwargs):
        if 2 != len(loc):
            raise ValueError("Location长度必须等于2")
        if not hasattr(Location, loc[0].upper()):
            raise TypeError(f"定位方式异常，请检查定位方式{loc}是否正确")
        new_loc = (getattr(Location, loc[0].upper()), loc[1])
        start = time.time()
        res = func(self, new_loc, *args, **kwargs)
        self.log.info(f"定位方式: {new_loc}, 耗时：{format(time.time() - start, '.3f')} 秒")
        return res

    return wrapper


class Location(object):
    ID = By.ID
    XPATH = By.XPATH
    LINK_TEXT = By.LINK_TEXT
    LINK = By.LINK_TEXT
    PARTIAL_LINK_TEXT = By.PARTIAL_LINK_TEXT
    PARTIAL_LINK = By.PARTIAL_LINK_TEXT
    NAME = By.NAME
    TAG_NAME = By.TAG_NAME
    TAG = By.TAG_NAME
    CLASS_NAME = By.CLASS_NAME
    CLASS = By.CLASS_NAME
    CSS_SELECTOR = By.CSS_SELECTOR
    CSS = By.CSS_SELECTOR


class Actions(ActionChains):

    def wait(self, times: float):
        self._actions.append(lambda: time.sleep(times))
        return self


class Browser(object):

    def __init__(self, driver, timeout=20, selenium_implicit_wait=10):
        self.driver = Engine(driver)()
        self.timeout = int(timeout)
        self.implicit_wait = int(selenium_implicit_wait)
        self.log = logger
        self.driver.implicitly_wait(self.implicit_wait)
        self._wait = WebDriverWait(self.driver, self.timeout)

    def open(self, url):
        self.driver.get(url)

    def back(self):
        self.driver.back()

    def forward(self):
        """前进到新窗口"""
        self.driver.forward()

    def maximize_window(self):
        self.driver.maximize_window()

    def set_window_size(self, width, height, windowHandle='current'):
        self.driver.set_window_size(width, height, windowHandle)

    def quit_browser(self):
        self.driver.quit()
        # os.system('taskkill /F /IM chrome.exe')

    def close_browser(self):
        self.driver.close()

    def title_is(self, expect_title: str):
        """
        判断title是否出现, 是则返回True, 否则返回False
        :param expect_title: 传入期待的title信息
        :return: bool
        """
        try:
            self._wait.until(EC.title_is(expect_title))
            return True
        except TimeoutException:
            return False

    def title_contains(self, expect_title_contains: str):
        """
        判断title是否包含某些字符,是则返回True, 否则返回False
        :param expect_title_contains: 期待包含的字符
        :return: bool
        """
        try:
            self._wait.until(EC.title_contains(expect_title_contains))
            return True
        except TimeoutException:
            return False

    def url_changes(self, url):
        return EC.url_changes(url)

    @decorator
    def find_element(self, loc: tuple):
        try:
            return self._wait.until(EC.presence_of_element_located(loc))
        except (NoSuchElementException, TimeoutException):
            self.log.error("%s 页面未找到元素 %s" % (self, loc))
            raise

    def click(self, loc: tuple):
        self.element_to_be_clickable(loc).click()

    def send_keys(self, loc: tuple, value: str):
        element = self.find_element(loc)
        element.clear()
        self.log.info(f"设置值：{value}")
        element.send_keys(value)

    def get_attribute(self, loc, name):
        value = self.find_element(loc).get_attribute(name)
        self.log.info(f"获取{name}属性值： {value}")
        return value

    def attribute_is(self, loc, name, text: str):
        return text == self.get_attribute(loc, name)

    def execute_script(self, script, *args):
        """
        执行js
        :param script:
        :return:
        """
        self.log.info(f"执行javascript： {script}")
        return self.driver.execute_script(script, *args)

    def foucs_element(self, loc: tuple):
        """
        聚焦元素
        :param loc:
        :return:
        """
        element = self.find_element(loc)
        self.execute_script("arguments[0].scrollIntoView();", element)

    def scroll_to_top(self, seconds=2):
        """
        滚动到顶部
        :return:
        """
        self.log.info("滚动到顶部")
        time.sleep(seconds)
        self.execute_script("window.scrollTo(0,0)")

    def scroll_to_bottom(self, seconds=2):
        """
        滚动到底部
        :return:
        """
        self.log.info("滚动到底部")
        time.sleep(seconds)
        self.execute_script("window.scrollTo(0,document.body.scrollHeight)")

    def scroll_to(self, x: int, y: int, seconds=2):
        """

        :param x:
        :param y:
        :return:
        """
        time.sleep(seconds)
        self.execute_script("window.scrollTo({}, {})".format(x, y))

    def select_by(self, loc: tuple, **kwargs):
        """

        :param loc: ("id", "定位器表达式")
        :param kwargs:
        :return:
        """
        element = self.find_element(loc)
        if "index" in kwargs.keys():
            return Select(element).select_by_index(int(kwargs.get("index")))
        elif "value" in kwargs.keys():
            return Select(element).select_by_value(kwargs.get("value"))
        elif "text" in kwargs.keys():
            return Select(element).select_by_value(kwargs.get("text"))
        else:
            raise Exception("key need in ['index', 'value', 'text']")

    @decorator
    def presence_of_element_located(self, loc: tuple):
        """
        判断元素是否定位到（元素不一定是可见）, 是则返回True, 否则返回False
        :param loc: ("id", "定位器表达式")
        :return:
        """
        try:
            self._wait.until(EC.presence_of_element_located(loc))
            return True
        except (NoSuchElementException, TimeoutException):
            return False

    @decorator
    def presence_of_all_elements_located(self, loc: tuple):
        """
        判断是否至少有一个元素存在于dom树中,是则返回True, 否则返回False
        :param loc: ("id", "定位器表达式")
        :return:
        """
        try:
            self._wait.until(EC.presence_of_all_elements_located(loc))
            return True
        except (NoSuchElementException, TimeoutException):
            return False

    @decorator
    def visibility_of_any_elements_located(self, loc: tuple):
        """
        判断是否至少有一个元素在页面中可见，是则返回True, 否则返回False
        :param loc: ("id", "定位器表达式")
        :return:
        """
        try:
            return self._wait.until(EC.visibility_of_any_elements_located(loc))
        except (NoSuchElementException, TimeoutException):
            return False

    @decorator
    def is_visibility(self, loc: tuple):
        """
        判断元素是否可见，可见返回本身, 否则返回False
        :param loc: ("id", "定位器表达式")
        :return:
        """
        try:
            return self._wait.until(EC.visibility_of_element_located(loc))
        except (NoSuchElementException, TimeoutException):
            return False

    @decorator
    def visibility_of(self, loc: tuple):
        """
        判断元素是否可见，如果可见则返回True,否则返回False
        :param loc: ("id", "定位器表达式")
        :return:
        """
        try:
            return self._wait.until(EC.visibility_of(self.driver.find_element(*loc)))
        except (NoSuchElementException, TimeoutException):
            return False

    @decorator
    def text_to_be_present_in_element(self, loc: tuple, text: str):
        """
        判断文本内容text是否出现在定位元素中,判断的是元素的text,是则返回True,否则返回False
        :param loc: ("id", "定位器表达式")
        :param text: 预期字符串
        :return:
        """
        try:
            self._wait.until(EC.text_to_be_present_in_element(loc, text))
            return True
        except (NoSuchElementException, TimeoutException):
            return False

    @decorator
    def text_to_be_present_in_element_value(self, loc: tuple, text: str):
        """
        判断文本内容text是否出现在定位元素的value属性值, 是则返回True,否则返回False
        :param loc: ("id", "定位器表达式")
        :param text: 预期字符串
        :return:
        """
        try:
            self._wait.until(EC.text_to_be_present_in_element_value(loc, text))
            return True
        except (NoSuchElementException, TimeoutException):
            return False

    @decorator
    def switch_to_frame(self, loc: tuple):
        """
        判断frame是否可以switch进去，如果可以的话，返回True并且switch进去，否则返回False
        :param loc: ("id", "定位器表达式")
        :return:
        """
        try:
            return self._wait.until(EC.frame_to_be_available_and_switch_to_it(loc))
        except (NoSuchElementException, TimeoutException):
            return False

    def switch_to_default_content(self):
        self.log.info("从frame切回主文档")
        self.driver.switch_to.default_content()

    def switch_handle(self, number: int):
        self.log.info("切换标签页")
        handles = self.driver.window_handles
        self.driver.switch_to.window(handles[number])

    @decorator
    def is_invisibility(self, loc: tuple):
        """
        判断元素不存在于dom或不可见，可见返回本身, 否则返回False
        :param loc: ("id", "定位器表达式")
        :return:
        """
        try:
            return self._wait.until(EC.invisibility_of_element_located(loc))
        except (NoSuchElementException, TimeoutException):
            return True

    @decorator
    def element_to_be_clickable(self, loc: tuple):
        """
        判断元素是否点击，可以点击返回本身, 否则raise
        :param loc: ("id", "定位器表达式")
        :return:
        """
        try:
            return self._wait.until(EC.element_to_be_clickable(loc))
        except (NoSuchElementException, TimeoutException):
            self.log.error("%s 页面未找到或无法点击元素 %s " % (self, loc))
            raise

    @decorator
    def staleness_of(self, loc: tuple):
        """
        判断某个元素是否仍在DOM中, 如果在规定时间内移除返回True, 否则返回False
        :param loc: ("id", "定位器表达式")
        :return:
        """
        try:
            return self._wait.until(EC.staleness_of(self.driver.find_element(*loc)))
        except (NoSuchElementException, TimeoutException):
            return True

    @decorator
    def element_to_be_selected(self, loc: tuple):
        """
        判断元素是否被选中，一般用在下拉列表, 是则返回True, 否则返回False
        :param loc: ("id", "定位器表达式")
        :return:
        """
        try:
            return self._wait.until(EC.element_to_be_selected(self.driver.find_element(*loc)))
        except (NoSuchElementException, TimeoutException):
            return False

    @decorator
    def element_located_to_be_selected(self, loc: tuple):
        """
        判断元素是否被选中，一般用在下拉列表, 是则返回True, 否则返回False
        :param loc: ("id", "定位器表达式")
        :return:
        """
        try:
            return self._wait.until(EC.element_located_to_be_selected(loc))
        except (NoSuchElementException, TimeoutException):
            return False

    @decorator
    def element_selection_state_to_be(self, loc: tuple, state=True):
        """
        判断某个元素的状态是否是给定的状态, 是则返回True, 否则返回False
        :param loc: ("id", "定位器表达式")
        :param state: bool
        :return:
        """
        try:
            return self._wait.until(EC.element_selection_state_to_be(self.driver.find_element(*loc), state))
        except (NoSuchElementException, TimeoutException):
            return False

    @decorator
    def element_located_selection_state_to_be(self, loc: tuple, state=True):
        """
        判断某个元素的状态是否是给定的状态, 是则返回True, 否则返回False
        :param loc: ("id", "定位器表达式")
        :param state: bool
        :return:
        """
        try:
            return self._wait.until(EC.element_located_selection_state_to_be(loc, state))
        except (NoSuchElementException, TimeoutException):
            return False

    def alert_is_present(self):
        """
        判断页面有无alert弹出框，有alert返回alert，无alert返回False
        :return:
        """
        try:
            return self._wait.until(EC.alert_is_present())
        except (NoSuchElementException, TimeoutException):
            return False

    def double_click(self, loc: tuple, times=0.5):
        """
        双击
        :param loc:
        :param times:
        :return:
        """
        element = self.find_element(loc)
        Actions(self.driver).wait(times).double_click(element).perform()

    def right_click(self, loc: tuple, times=0.5):
        """
        右击
        :param loc:
        :param times:
        :return:
        """
        element = self.find_element(loc)
        Actions(self.driver).wait(times).context_click(element).perform()

    def move_to_element(self, loc: tuple, times=0.5):
        """

        :param loc:
        :param times:
        :return:
        """
        element = self.find_element(loc)
        Actions(self.driver).wait(times).move_to_element(element).perform()


if __name__ == '__main__':
    pass
    # import os, time
    #
    # builder = Browser("Chrome")
    # builder.open("http://www.baidu.com")
    # builder.maximize_window()
    # print(builder.title_is("百度一下，你就知道"))
    # print(builder.title_contains("百度一下"))
    # print(builder.presence_of_element_located(('ID', 'kw')))
    # print(builder.visibility_of_element_located(('xpaTH', '//*[@id="su"]')))
    # print(builder.visibility_of(('xpaTH', '//*[@id="su"]')))
    # print(builder.presence_of_all_elements_located(('xpaTh', '//*[@id="su"]')))
    # print(builder.text_to_be_present_in_element(('xpaTh', '//*[@id="s-top-loginbtn"]'), "登录"))
    # print(builder.text_to_be_present_in_element_value(('xpaTh', '//*[@id="su"]'), "百度一下"))
    # print(builder.invisibility_of_element_located(('xpaTh', '//*[@id="su"]')))
    # print(builder.element_to_be_clickable(('xpaTh', '//*[@id="su"]')))
    # print(builder.staleness_of(('xpaTh', '//*[@id="su"]')))
    # print(builder.element_located_selection_state_to_be(('xpaTh', '//*[@id="su"]'), True))
    # builder.click(('xpaTh', '//*[@id="s-top-loginbtn"]'))
    # builder.send_keys(('ID', 'kw'), "selenium actionchains")
    # builder.click(('xpaTh', '//*[@id="su"]'))
    # builder.get_attribute(('xpath', "//*[@id='wrapper']//div[@id='u']/a[1]"), 'class')
    # builder.click(("xpath", '//div[@class="FYB_RD"]//span[text()="换一换"]'))
    # builder.move_to_element(("css", '.pf'))
    # builder.click(("link", "搜索设置"))
    # builder.scroll_to_bottom()
    # builder.scroll_to_top()
    # builder.scroll_to(100, 400)
    # print(builder.alert_is_present())
    # builder.quit_browser()
    # os.system('taskkill /F /IM chromedriver.exe')

    # xpath = 'CSS_SELECTOR'
    #
    # if hasattr(Location, xpath.upper()):
    #     print(getattr(Location, xpath.upper()))
    # xapth = ('id', 'kw')
    # print(len(xapth))
