#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @File       :  engine.py [Python]
# @Date       :  2021/11/21
# @Author     :  mitool
# @Email      :  
# @Software   :  PyCharm
# @Version    :  1.0
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------

import platform
import threading
from selenium import webdriver

from webAuto.cores.utils.iniHelper import ini
from webAuto.cores.utils.fileHelper import File
from webAuto import settings

'''
def synchronized(func):
    func.__lock__ = threading.Lock()

    def lock_func(*args, **kwargs):
        with func.__lock__:
            return func(*args, **kwargs)

    return lock_func


class Singleton(object):
    """
    单例模式
    """
    instance = None

    @synchronized
    def __new__(cls, *args, **kwargs):
        if cls.instance is None:
            cls.instance = object.__new__(cls, *args, **kwargs)
        return cls.instance
'''


class Singleton(object):
    _instance_lock = threading.Lock()

    def __init__(self):
        pass

    def __new__(cls, *args, **kwargs):
        if not hasattr(Singleton, "_instance"):
            with Singleton._instance_lock:
                if not hasattr(Singleton, "_instance"):
                    Singleton._instance = object.__new__(cls)
        return Singleton._instance


class DriverPath(object):
    """获取webdriver执行文件路径"""

    def __init__(self, browser):
        self.maps = ini(File.join(settings.CONF_PATH, 'maps.ini'))
        self.system = platform.system().lower()
        self.machine = platform.architecture()[0]
        self.file = self.maps.get(self.system, browser)

    def __call__(self):
        path = File.join(settings.DRIVER_PATH, self.machine, self.system, self.file)
        if not (File.exists(path) and File.isfile(path)):
            raise FileNotFoundError
        return path


class Options(object):
    # TODO

    pass


class Engine(Singleton):

    def __init__(self, driver):
        super(Engine).__init__()
        self.browser = None
        if driver is not None and hasattr(webdriver, driver):
            self.browser = driver
        if self.browser is None:
            raise ValueError("browser --> {0} is None".format(self.browser))
        self.exec_path = DriverPath(self.browser)()

    def __call__(self):
        builder = getattr(webdriver, self.browser)
        return builder(self.exec_path)


if __name__ == '__main__':
    pass
