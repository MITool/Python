#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @File       :  conftest.py [Python]
# @Date       :  2021/12/11
# @Author     :  mitool
# @Email      :  
# @Software   :  PyCharm
# @Version    :  1.0
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------

import platform
import os
import pytest

from webAuto.cores.webui.browser import Browser

browsers = (
    'BlackBerry',
    'Chrome',
    'Edge',
    'Firefox',
    'Ie',
    'Marionette',
    'PhantomJS',
    'Remote',
    'Safari',
)


@pytest.fixture(autouse=True)
def drivers(request, settings):
    browser, selenium_timeout, selenium_implicit_wait, base_url = settings
    driver = Browser(browser, selenium_timeout, selenium_implicit_wait)
    driver.maximize_window()
    driver.open(base_url)
    driver.log.info("====================开始测试=====================")
    yield driver
    driver.log.info("====================结束测试=====================")
    driver.quit_browser()


@pytest.fixture(scope="session", autouse=True)
def kill_driver(request):
    def fn():
        if "windows" == platform.system().lower():
            os.system('taskkill /F /IM chromedriver.exe')

    request.addfinalizer(fn)


@pytest.fixture(scope="session")
def settings(request):
    conf = request.config
    browser = conf.getini('browser')
    if browser is None and browser not in browsers:
        raise ValueError('browser to run tests against ({0})'.format(', '.join(browsers)))
    selenium_timeout = conf.getini('selenium_timeout')
    selenium_implicit_wait = conf.getini('selenium_implicit_wait')
    base_url = conf.getoption("base_url")

    return browser, selenium_timeout, selenium_implicit_wait, base_url


def pytest_addoption(parser):
    parser.addini("browser", help="browser for the application under test.")
    parser.addini("selenium_timeout", help="set webdriver timeout.")
    parser.addini("selenium_implicit_wait", help="set selenium implicit wait.")
    parser.addoption(
        '--browser',
        action='append',
        choices=browsers,
        dest='browsers',
        metavar='BROWSER',
        help='browser to run tests against ({0})'.format(', '.join(browsers)))
