#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @File       :  __init__.py.py [Python]
# @Date       :  2021/11/21
# @Author     :  mitool
# @Email      :  
# @Software   :  PyCharm
# @Version    :  1.0
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------

import os
from functools import wraps

from webAuto.cores.utils.iniHelper import ini
from webAuto.cores.utils.fileHelper import File

LOC_PATH = os.path.dirname(os.path.realpath(__file__))
xpath = ini(File.join(LOC_PATH, 'data', 'loc.ini')).as_dict()

def filter_loc(func):
    @wraps(func)
    def inner(self, *args, **kwargs):
        # print(f"{func}所属类： {self.__class__.__name__}")
        # print(f"{func}名称： {func.__name__}")
        cls_name = self.__class__.__name__.lower()
        loc = xpath.get(cls_name, None).get(func.__name__, None)
        # print(eval(loc))
        if loc is None:
            raise ValueError("loc is not None")
        return func(self, eval(loc), *args, **kwargs)

    return inner
