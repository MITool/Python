#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @File       :  test_login.py [Python]
# @Date       :  2021/12/15
# @Author     :  mitool
# @Email      :  
# @Software   :  PyCharm
# @Version    :  1.0
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------

import pytest

from webAuto.woc88.page.homePage import Home
from webAuto.woc88.page.loginPage import Login


class TestLogin(object):

    @pytest.mark.smoke
    @pytest.mark.parametrize(['user', 'pwd'], [
        ("18716695058", "bai19891023"),
        ("18716695058", "bai19891024"),
    ])
    def test_login(self, drivers, user, pwd):
        home = Home(drivers)
        home.login()
        login = Login(drivers)
        login.user(keywords=user)
        login.password(keywords=pwd)
        login.login()
