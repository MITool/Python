#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  options.py [Python]
# @Version    :  python3.6
# @Author     :  mitool
# @Date       :  2018/11/30
# @IDE        :  PyCharm
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------
import re
import os
import string
import zipfile
from selenium import webdriver
from webUI.libs.common import File
from webUI.libs import DOWNLOAD_PATH, BACKUP,ARCHIVED,CHROME_PROXY_ZIP_PATH, PROXY_LIST


class Proxy(object):

    def __init__(self, proxy_string):
        self.proxy_string = proxy_string
        self.proxy_auth = False
        self.proxy_user = None
        self.proxy_pwd = None

        if self.proxy_string:
            if "@" in self.proxy_string:
                # Format => username:password@hostname:port
                try:
                    user_pwd = self.proxy_string.split('@')[0]
                    self.proxy_string = self.proxy_string.split('@')[1]
                    self.proxy_user = user_pwd.split(':')[0]
                    self.proxy_pwd = user_pwd.split(':')[1]
                except Exception:
                    raise Exception(
                        'The format for using a proxy server with authentication is:\n'
                        ' "username:password@hostname:port". \n'
                        'If using a proxy server without auth, the format is: "hostname:port".')
            self.proxy_string = self._get_proxy(self.proxy_string)
            if self.proxy_string and self.proxy_user and self.proxy_pwd:
                self.proxy_auth = True

    @staticmethod
    def valid_url(url):
        regex = re.compile(
            r'^(?:http)s?://'  # http:// or https://
            r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+'
            r'(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|'  # domain...
            r'localhost|'  # localhost...
            r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'  # ...or ip
            r'(?::\d+)?'  # optional port
            r'(?:/?|[/?]\S+)$', re.IGNORECASE)
        if regex.match(url) or url == 'about:blank' or url == 'data:,':
            return True
        return False

    def valid_proxy(self, proxy_string):
        valid = False
        val_ip = re.match(r'^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}:\d+$', proxy_string)

        if val_ip:
            proxy_string = val_ip.group()
            valid = True
        else:
            if proxy_string.startswith('http://'):
                proxy_string = proxy_string.split('http://')[1]
            elif proxy_string.startswith('https://'):
                proxy_string = proxy_string.split('https://')[1]
            elif '://' in proxy_string:
                proxy_string = proxy_string.split('://')[1]
            chunks = proxy_string.split(':')
            if len(chunks) == 2:
                if re.match(r'^\d+$', chunks[1]):
                    if self.valid_url('http://' + proxy_string):
                        valid = True
        if not valid:
            proxy_string = None
        return proxy_string

    def _get_proxy(self, proxy_string):
        if proxy_string in PROXY_LIST.keys():
            proxy_string = PROXY_LIST.get(proxy_string)
            if not proxy_string:
                return None
        return self.valid_proxy(proxy_string)


class Options(Proxy):

    def __init__(self, proxy_string):
        super(Options, self).__init__(proxy_string)
        self._download_path = DOWNLOAD_PATH
        self.proxy_host = None
        self.proxy_port = None
        if self.proxy_string is not None:
            self.proxy_host = self.proxy_string.split(':')[0]
            self.proxy_port = self.proxy_string.split(':')[1]

    @property
    def download_path(self):
        File.reset_folder(self._download_path, BACKUP, ARCHIVED)
        return self._download_path

    @download_path.setter
    def download_path(self, download_path):
        if not os.path.exists(download_path):
            os.mkdir(download_path)
        self._download_path = download_path

    def __call__(self):
        raise NotImplementedError("Please implement this method")


class Chrome(Options):

    def __init__(self, proxy_string):
        super(Chrome, self).__init__(proxy_string)
        self._proxy_zip = CHROME_PROXY_ZIP_PATH
        self.options = webdriver.ChromeOptions()

    @property
    def proxy_zip(self):
        return self._proxy_zip

    @proxy_zip.setter
    def proxy_zip(self, path):
        """
        :param path: absolute path
        :return:
        """
        self._proxy_zip = path

    def __call__(self):
        prefs = {
            "download.default_directory": self.download_path,
            "credentials_enable_service": False,
            "profile": {
                "password_manager_enabled": False
                # "managed_default_content_settings.images":2
            }
        }
        self.options.add_experimental_option("prefs", prefs)
        self.options.add_argument('--start-maximized')
        self.options.add_argument("--test-type")
        self.options.add_argument("--no-first-run")
        self.options.add_argument("--ignore-certificate-errors")
        self.options.add_argument("--allow-file-access-from-files")
        self.options.add_argument("--allow-insecure-localhost")
        self.options.add_argument("--allow-running-insecure-content")
        self.options.add_argument("--disable-infobars")
        self.options.add_argument("--disable-save-password-bubble")
        self.options.add_argument("--disable-single-click-autofill")
        self.options.add_argument("--disable-translate")
        self.options.add_argument("--disable-web-security")
        if self.proxy_string:
            if self.proxy_auth:
                self.options.add_extension(self.create_proxyauth_extension())
            self.options.add_argument('--proxy-server={}'.format(self.proxy_string))
        return self.options

    def create_proxyauth_extension(self, scheme='http'):
        """Proxy Auth Extension

        args:
            proxy_host (str): domain or ip address, ie proxy.domain.com
            proxy_port (int): port
            proxy_user (str): auth username
            proxy_pwd (str): auth password
        kwargs:
            scheme (str): proxy scheme, default http
            proxy_zip (str): absolute path of the extension

        return str -> proxy_zip(plugin_path)
        """

        manifest_json = """
        {
            "version": "1.0.0",
            "manifest_version": 2,
            "name": "Chrome Proxy",
            "permissions": [
                "proxy",
                "tabs",
                "unlimitedStorage",
                "storage",
                "<all_urls>",
                "webRequest",
                "webRequestBlocking"
            ],
            "background": {
                "scripts": ["background.js"]
            },
            "minimum_chrome_version":"22.0.0"
        }
        """

        background_js = string.Template(
            """
            var config = {
                    mode: "fixed_servers",
                    rules: {
                      singleProxy: {
                        scheme: "${scheme}",
                        host: "${host}",
                        port: parseInt(${port})
                      },
                      bypassList: ["foobar.com"]
                    }
                  };
    
            chrome.proxy.settings.set({value: config, scope: "regular"}, function() {});
    
            function callbackFn(details) {
                return {
                    authCredentials: {
                        username: "${username}",
                        password: "${password}"
                    }
                };
            }
    
            chrome.webRequest.onAuthRequired.addListener(
                        callbackFn,
                        {urls: ["<all_urls>"]},
                        ['blocking']
            );
            """
        ).substitute(
            host=self.proxy_host,
            port=int(self.proxy_port),
            username=self.proxy_user,
            password=self.proxy_pwd,
            scheme=scheme,
        )
        with zipfile.ZipFile(self.proxy_zip, 'w') as zp:
            zp.writestr("manifest.json", manifest_json)
            zp.writestr("background.js", background_js)

        return self.proxy_zip

    def remove_proxy_zip_if_present(self):
        """ Remove Chrome extension zip file used for proxy server authentication.
            Used in the implementation of https://stackoverflow.com/a/35293284
            for https://stackoverflow.com/questions/12848327/
        """
        try:
            if os.path.exists(self.proxy_zip):
                os.remove(self.proxy_zip)
        except Exception:
            raise Exception


class Firefox(Options):

    def __init__(self, proxy_string):
        super(Firefox, self).__init__(proxy_string)
        self.profile = webdriver.FirefoxProfile()

    def __call__(self):
        self.profile.accept_untrusted_certs = True
        self.profile.set_preference("reader.parse-on-load.enabled", False)
        self.profile.set_preference("pdfjs.disabled", True)
        self.profile.set_preference("app.update.auto", False)
        self.profile.set_preference("app.update.enabled", False)
        self.profile.set_preference("security.mixed_content.block_active_content", False)
        self.profile.set_preference("security.csp.enable", False)
        self.profile.set_preference("browser.download.manager.showAlertOnComplete", False)
        self.profile.set_preference("browser.privatebrowsing.autostart", True)
        self.profile.set_preference("browser.download.panel.shown", False)
        self.profile.set_preference("browser.download.animateNotifications", False)
        self.profile.set_preference("browser.download.dir", self.download_path)
        self.profile.set_preference("browser.download.folderList", 2)
        self.profile.set_preference(
            "browser.helperApps.neverAsk.saveToDisk",
            ("application/pdf, application/zip, application/octet-stream, "
             "text/csv, text/xml, application/xml, text/plain, "
             "text/octet-stream, "
             "application/"
             "vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
        if self.proxy_string:
            self.add_proxy_extension()
        return self.profile

    def add_proxy_extension(self):
        self.profile.set_preference("network.proxy.type", 1)
        self.profile.set_preference("network.proxy.http", self.proxy_host)
        self.profile.set_preference("network.proxy.http_port", int(self.proxy_port))
        # self.profile.set_preference("network.proxy.https",self.proxy_host)
        # self.profile.set_preference("network.proxy.https_port",int(self.proxy_port))
        self.profile.set_preference("network.proxy.ssl", self.proxy_host)
        self.profile.set_preference("network.proxy.ssl_port", int(self.proxy_port))
        # self.profile.set_preference("network.proxy.ftp", self.proxy_host)
        # self.profile.set_preference("network.proxy.ftp_port", int(self.proxy_port))
        # self.profile.set_preference("network.proxy.socks", self.proxy_host)
        # self.profile.set_preference("network.proxy.socks_port", int(self.proxy_port))


class Ie(Options):

    def __init__(self, proxy_string):
        super(Ie, self).__init__(proxy_string)
        self.options = webdriver.IeOptions()

    def __call__(self):
        self.options.ignore_protected_mode_settings = False
        self.options.ignore_zoom_level = True
        self.options.require_window_focus = False
        self.options.native_events = True
        self.options.full_page_screenshot = True
        self.options.persistent_hover = True
        return self.options
