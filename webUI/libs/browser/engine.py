#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  engine.py [Python]
# @Version    :  python3.6
# @Author     :  mitool
# @Date       :  2018/11/28
# @IDE        :  PyCharm
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------
import platform
import warnings
from selenium import webdriver
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

from webUI import _settings
from webUI.libs.common import File
from webUI.libs.browser import options
from webUI.libs.driverManager import _driver
from webUI.libs.common import logHelper as log

class Engine(object):

    def __init__(self, browser, browser_bit, server, port):
        self.browser = browser
        self.browser_bit = browser_bit
        self.plat = _settings.OS
        self._driver_path = _settings.DRIVER_PATH
        self._exec_path = None
        self.drconf = _settings.drconf
        self.drivermap = _settings.drivermap
        self.address = "http://{0}:{1}/wd/hub".format(server, port)

    @property
    def exec_path(self):
        if self._exec_path is not None:
            return self._exec_path
        if _settings.CUSTOM_DRIVER:
            return _settings.CUSTOM_DRIVER
        if self.drconf.get(self.browser):
            key = ''.join([self.plat, str(self.browser_bit)])
            return File.join(self._driver_path, self.drconf.get(self.browser).get(key))
        return None

    @exec_path.setter
    def exec_path(self, exec_path):
        self._exec_path = exec_path

    def local(self):
        raise NotImplementedError("Please implement this method")

    def remote(self):
        raise NotImplementedError("Please implement this method")

    def download_driver(self):
        if not _settings.VALID_DOWNLOAD:
            log.info('config[driverManager][valid_download] = {0}, do not download webdriver.'.format(
                _settings.VALID_DOWNLOAD))
            return
        if 'edge' == self.browser:
            if not ('windows' == self.plat):
                raise Exception("EDGE Browser is for Windows-based operating systems only!")
            self.browser_version = platform.version().split('.')[2]
        if 'ie' == self.browser:
            if not ('windows' == self.plat):
                raise Exception("IE Browser is for Windows-based operating systems only!")
            import selenium
            self.browser_version = selenium.__version__
            self.browser_bit = '32'

        if self.browser_version not in self.drivermap.get(self.browser).keys():
            raise Exception('browserVersion set Error, please check Browser[{0}], Version[{1}]'
                            .format(self.browser, self.browser_version))
        dri_version = self.drivermap.get(self.browser).get(self.browser_version)
        cls = getattr(_driver, self.browser)
        obj = cls(self.plat, self.browser_bit, dri_version, self.drconf)
        obj.install(self._driver_path)


class phantomjs(Engine):

    def __init__(self, headless, browser, browser_bit, server, port, proxy_string):
        super(phantomjs, self).__init__(browser, browser_bit, server, port)

    def local(self):
        with warnings.catch_warnings():
            # Ignore "PhantomJS has been deprecated" UserWarning
            warnings.simplefilter("ignore", category=UserWarning)
            if self.exec_path and File.exists(self.exec_path):
                File.make_executable_if_not(self.exec_path)
                return webdriver.PhantomJS(executable_path=self.exec_path)
            return webdriver.PhantomJS()

    def remote(self):
        with warnings.catch_warnings():
            # Ignore "PhantomJS has been deprecated" UserWarning
            warnings.simplefilter("ignore", category=UserWarning)
            return webdriver.Remote(command_executor=self.address,
                                    desired_capabilities=DesiredCapabilities.PHANTOMJS)


class opera(Engine):

    def __init__(self, headless, browser, browser_bit, server, port, proxy_string):
        super(opera, self).__init__(browser, browser_bit, server, port)

    def local(self):
        if self.exec_path and File.exists(self.exec_path):
            File.make_executable_if_not(self.exec_path)
            return webdriver.Opera(executable_path=self.exec_path)
        return webdriver.Opera()

    def remote(self):
        return webdriver.Remote(command_executor=self.address,
                                desired_capabilities=DesiredCapabilities.OPERA)


class safari(Engine):

    def __init__(self, headless, browser, browser_bit, server, port, proxy_string):
        super(safari, self).__init__(browser, browser_bit, server, port)

    def local(self):
        return webdriver.Safari()

    def remote(self):
        return webdriver.Remote(command_executor=self.address,
                                desired_capabilities=DesiredCapabilities.SAFARI)


class edge(Engine):

    def __init__(self, headless, browser, browser_bit, server, port, proxy_string):
        super(edge, self).__init__(browser, browser_bit, server, port)
        self._cap = DesiredCapabilities.EDGE.copy()

    def remote(self):
        return webdriver.Remote(command_executor=self.address,
                                desired_capabilities=self._cap)

    def local(self):
        if not ('windows' == self.plat):
            raise Exception("EDGE Browser is for Windows-based operating systems only!")
        if self.exec_path and File.exists(self.exec_path):
            File.make_executable_if_not(self.exec_path)
            return webdriver.Ie(capabilities=self._cap,
                                executable_path=self.exec_path)
        return webdriver.Ie(capabilities=self._cap)


class ie(Engine):

    def __init__(self, headless, browser, browser_bit, server, port, proxy_string):
        super(ie, self).__init__(browser, browser_bit, server, port)
        self.options = options.Ie(proxy_string)()
        self._cap = DesiredCapabilities.INTERNETEXPLORER.copy()

    def remote(self):
        return webdriver.Remote(command_executor=self.address,
                                desired_capabilities=self._cap,
                                options=self.options)

    def local(self):
        if not ('windows' == self.plat):
            raise Exception("IE Browser is for Windows-based operating systems only!")
        if self.exec_path and File.exists(self.exec_path):
            File.make_executable_if_not(self.exec_path)
            return webdriver.Ie(options=self.options,
                                executable_path=self.exec_path)
        return webdriver.Ie(options=self.options)


class firefox(Engine):

    def __init__(self, headless, browser, browser_bit, server, port, proxy_string):
        super(firefox, self).__init__(browser, browser_bit, server, port)
        self.headless = headless
        self.profile = options.Firefox(proxy_string)()
        self._cap = DesiredCapabilities.FIREFOX.copy()

    def local(self):
        try:
            try:
                if self.headless:
                    self._cap['moz:firefoxOptions'] = ({'args': ['-headless']})
                # Use Geckodriver for Firefox if it's on the PATH
                if self.exec_path and File.exists(self.exec_path):
                    File.make_executable_if_not(self.exec_path)
                    return webdriver.Firefox(firefox_profile=self.profile,
                                             capabilities=self._cap,
                                             executable_path=self.exec_path)
                return webdriver.Firefox(firefox_profile=self.profile,
                                         capabilities=self._cap)
            except WebDriverException:
                # Don't use Geckodriver: Only works for old versions of Firefox
                self._cap['marionette'] = False
                return webdriver.Firefox(firefox_profile=self.profile,
                                         capabilities=self._cap)
        except Exception as e:
            if self.headless: raise Exception(e)
            if self.exec_path and File.exists(self.exec_path):
                File.make_executable_if_not(self.exec_path)
                return webdriver.Firefox(executable_path=self.exec_path)
            return webdriver.Firefox()

    def remote(self):
        try:
            if self.headless:
                self._cap['moz:firefoxOptions'] = ({'args': ['-headless']})
            # Use Geckodriver for Firefox if it's on the PATH
            return webdriver.Remote(command_executor=self.address,
                                    desired_capabilities=self._cap,
                                    browser_profile=self.profile)
        except WebDriverException:
            # Don't use Geckodriver: Only works for old versions of Firefox
            self._cap['marionette'] = False
            return webdriver.Remote(command_executor=self.address,
                                    desired_capabilities=self._cap,
                                    browser_profile=self.profile)


class chrome(Engine):

    def __init__(self, headless, browser, browser_bit, server, port, proxy_string):
        super(chrome, self).__init__(browser, browser_bit, server, port)
        self.headless = headless
        _option = options.Chrome(proxy_string)
        self.proxy_auth = _option.proxy_auth
        self.options = _option()

        if 'chrome' != self.browser and self.proxy_auth:
            raise Exception(
                "Chrome is required when using a proxy server that has authentication!\n"
                " (If using a proxy server without auth, either Chrome or Firefox may be used.)")

    def local(self):
        try:
            if self.headless:
                # Headless Chrome doesn't support extensions, which are
                # required when using a proxy server that has authentication.
                # Instead,  will use PyVirtualDisplay when not
                # using Chrome's built-in headless mode. See link for details:
                # https://bugs.chromium.org/p/chromium/issues/detail?id=706008
                if not self.proxy_auth:
                    self.options.add_argument("--headless")
                self.options.add_argument("--disable-gpu")
                self.options.add_argument("--no-sandbox")
            if self.exec_path and File.exists(self.exec_path):
                File.make_executable_if_not(self.exec_path)
                return webdriver.Chrome(executable_path=self.exec_path,
                                        options=self.options)
            return webdriver.Chrome(options=self.options)
        except Exception as e:
            if self.headless: raise Exception(e)
            if self.exec_path and File.exists(self.exec_path):
                File.make_executable_if_not(self.exec_path)
                return webdriver.Chrome(executable_path=self.exec_path)
            return webdriver.Chrome()

    def remote(self):
        if self.headless:
            self.options.add_argument("--headless")
            self.options.add_argument("--disable-gpu")
            self.options.add_argument("--no-sandbox")
        return webdriver.Remote(
            command_executor=self.address,
            desired_capabilities=self.options.to_capabilities())