#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  browser.py [Python]
# @Version    :  python3.6
# @Author     :  mitool
# @Date       :  2018/12/1
# @IDE        :  PyCharm
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------

from webUI import _settings


class Params(object):

    def __init__(self):
        self._browser         = _settings.BROWSER
        self._browser_bit     = _settings.BROWSER_VERSION[1]
        self._browser_version = _settings.BROWSER_VERSION[0]
        self._headless        = False
        self._remote          = False

    @property
    def browser(self):
        if self._browser not in _settings.VALID_BROWSERS:
            raise Exception('webdriver support Error, validete browsers :{}'.format(
                str(_settings.VALID_BROWSERS)))
        return self._browser

    @browser.setter
    def browser(self, browser):
        self._browser = browser

    @property
    def browser_version(self):
        return self._browser_version

    @browser_version.setter
    def browser_version(self, browser_version):
        self._browser_version = browser_version

    @property
    def browser_bit(self):
        return self._browser_bit

    @browser_bit.setter
    def browser_bit(self, browser_bit):
        self._browser_bit = browser_bit

    @property
    def headless(self):
        return self._headless

    @headless.setter
    def headless(self, headless):
        if isinstance(headless, bool):
            raise ValueError('must set "True" or "False".')
        self._headless = headless

    def get_domain_url(self, url):
        """
        Use this to convert a url like this:
        https://blog.xkcd.com/2014/07/22/what-if-book-tour/
        Into this:
        https://blog.xkcd.com
        """
        url_header = url.split('://')[0]
        simple_url = url.split('://')[1]
        base_url = simple_url.split('/')[0]
        return '{0}://{1}'.format(url_header, base_url)

