#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  logHelper.py [Python]
# @Version    :  python3.6
# @Author     :  mitool
# @Date       :  2018/11/5
# @IDE        :  PyCharm
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------
import os
import logging
import logging.config
import time
from webUI.libs.common.fileHelper import File
from webUI.settings import log_level, log_path


class logHelper(object):

    def __init__(self):
        File.mkdirs(log_path)
        self.log_file = "{}.log".format(time.strftime("%Y-%m-%d", time.localtime()))
        self.log_conf = {
            "version": 1,
            "disable_existing_loggers": False,
            "formatters": {
                "simple": {
                    '()': 'colorlog.ColoredFormatter',
                    'format': '%(log_color)s%(asctime)s - %(filename)-20s - [line:%(lineno)4d] - %(levelname)-8s %(blue)s : %(message)s',
                    'log_colors': {
                        'DEBUG': 'cyan',
                        'INFO': 'green',
                        'WARNING': 'yellow',
                        'ERROR': 'red',
                        'CRITICAL': 'red',
                    }
                },
                'standard': {
                    'format': '%(asctime)s - %(filename)-20s[line:%(lineno)4d] - %(levelname)-8s : %(message)s'
                },
            },

            "handlers": {
                "console": {
                    "class": "colorlog.StreamHandler",
                    "level": "INFO",
                    "formatter": "simple",
                    "stream": "ext://sys.stdout"
                },
                "default": {
                    "class": "logging.handlers.RotatingFileHandler",
                    "level": "INFO",
                    "formatter": "standard",
                    "filename": os.path.join(log_path, self.log_file),
                    'mode': 'w+',
                    "maxBytes": 1024 * 1024 * 50,  # 50 MB
                    "backupCount": 20,
                    "encoding": "utf8"
                },
                "error_file_handler": {
                    "class": "logging.handlers.RotatingFileHandler",
                    "level": "ERROR",
                    "formatter": "standard",
                    "filename": os.path.join(log_path, "errors.log"),
                    "maxBytes": 1024 * 1024 * 10,  # 10 MB
                    "backupCount": 20,
                    "encoding": "utf8"
                }
            },
            "root": {
                'handlers': ['default', 'console', 'error_file_handler'],
                'level': log_level,
                'propagate': False
            }
        }

    def __call__(self, name):
        try:
            logging.config.dictConfig(self.log_conf)
        except ValueError:
            self.log_conf['formatters']['simple'] = {
                'format': '%(asctime)s - %(filename)-20s[line:%(lineno)4d] - %(levelname)-8s : %(message)s'}
            self.log_conf['handlers']['console']['class'] = "logging.StreamHandler"
            logging.config.dictConfig(self.log_conf)
        return logging.getLogger(name)


logger = logHelper()