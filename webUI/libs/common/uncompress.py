#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  uncompress.py [Python]
# @Version    :  python3.6
# @Author     :  mitool
# @Date       :  2018/11/26
# @IDE        :  PyCharm
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------

import os
import tarfile
import zipfile
from .fileHelper import File
from AutoFramework.exception import exceptions


class uncompress(object):
    def __init__(self, file, to_dir=None):
        self.file = file
        self.to_dir = to_dir if to_dir is not None else os.path.dirname(os.path.abspath(file))

    def extract(self):
        raise NotImplementedError

    def __del__(self):
        File.remove(self.file)


class zip(uncompress):

    def __init__(self, file, to_dir):
        super(zip, self).__init__(file, to_dir)

    def extract(self):
        if not zipfile.is_zipfile(self.file):
            raise exceptions.FileTypeError(self.file)
        try:
            with zipfile.ZipFile(self.file, 'r') as zf:
                zf.extractall(self.to_dir)
        except Exception:
            raise exceptions.Error('Error extract zip_file : {0}'.format(self.file))

class gz(uncompress):

    def __init__(self, file, to_dir):
        super(gz, self).__init__(file, to_dir)


    def extract(self):
        if not tarfile.is_tarfile(self.file):
            raise exceptions.FileTypeError(self.file)
        try:
            with tarfile.open(self.file) as tar:
                tar.extractall(self.to_dir)
        except Exception:
            raise exceptions.Error('Error extract tar_file : {0}'.format(self.file))



