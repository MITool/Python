#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  fileHelper.py [Python]
# @Version    :  python3.6
# @Author     :  mitool
# @Date       :  2018/11/19
# @IDE        :  PyCharm
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------

import os
import shutil
import time

class File(object):

    @staticmethod
    def exists(path):
        return os.path.exists(path)

    @staticmethod
    def isdir(path):
        return os.path.isdir(path)

    @staticmethod
    def isfile(file):
        return os.path.isfile(file)

    @staticmethod
    def join(path, *paths):
        return os.path.join(path, *paths)

    @staticmethod
    def mkdirs(path):
        try:
            if not os.path.exists(path):
                os.makedirs(path)
                return True
        except OSError:
            return False

    @staticmethod
    def move(src, aim):
        try:
            shutil.move(src, aim)
            return True
        except OSError:
            return False

    @staticmethod
    def copyfile(src, aim):
        '''
        复制单个文件，返回是否成功
        :param src: (unicode) 源文件名
        :param aim: (unicode) 目标文件名
        :return: (bool) ->True：复制成功,False:复制失败
        '''
        try:
            shutil.copyfile(src, aim)
            return True
        except OSError:
            return False

    @staticmethod
    def copytree(src, aim):
        '''
        复制目录树，返回是否成功
        :param src: (unicode) 源文件名
        :param aim: (unicode) 目标文件名
        :return: (bool) ->True：复制成功,False:复制失败
        '''
        try:
            shutil.copytree(src, aim)
            return True
        except OSError:
            return False

    @staticmethod
    def rname(old, new):
        """
        重命名
        :param old: （unicode) 源文件名
        :param new:  (unicode) 目标文件名
        :return: (bool) 是否成功 -> True:成功,False:失败
        """
        try:
            os.rename(old, new)
            return True
        except OSError:
            return False

    @staticmethod
    def remove(file):
        """
        删除文件
        :param file:  (str) 文件名
        :return: （bool) 是否成功 -> True：成功 False:失败
        """
        if os.path.exists(file):
            try:
                os.remove(file)
                return True
            except OSError:
                return False
        else:
            return True

    @staticmethod
    def rmdir(path):
        """
        删除目录或文件
        :param dir: (str)  目录名
        :return: (bool) 是否成功 -> True:成功 False:失败
        """
        try:
            shutil.rmtree(path)
            return True
        except OSError:
            return False

    @staticmethod
    def listdir(path):
        return os.listdir(path)

    @staticmethod
    def reset_folder(folder, backup_folder=None, is_backup=True):
        '''
        Clears the folder.
        If backup is set to True, backup it.
        If False, only the driverManager from the most recent run will be saved locally.
        '''
        backup = backup_folder if backup_folder is not None else 'backups'
        if File.exists(folder) and not File.listdir(folder) == []:
            backup_ = File.join(folder, '..', backup)
            File.mkdirs(backup_)
            sub_folder = File.join(backup_, int(time.time()))
            File.move(folder, sub_folder)
            File.mkdirs(folder)
            if not is_backup:
                File.rmdir(sub_folder)


    @staticmethod
    def make_executable_if_not(file_path):
        # Verify driverManager has executable permissions. If not, add them.
        permissions = oct(os.stat(file_path)[0])[-3:]
        if '4' in permissions or '6' in permissions:
            # We want at least a '5' or '7' to make sure it's executable
            mode = os.stat(file_path).st_mode
            mode |= (mode & 0o444) >> 2  # copy R bits to X
            os.chmod(file_path, mode)




