#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  driver.py [Python]
# @Version    :  python3.6
# @Author     :  mitool
# @Date       :  2019/4/18
# @IDE        :  PyCharm
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------
import os
import requests
import filetype


from webUI.libs.common.fileHelper import File
from webUI.libs.common.logHelper import logger
from webUI.libs.common import uncompress
from AutoFramework.exception import exceptions

log = logger(__name__)


class Driver(object):

    def __init__(self, plat, browser_bit, driver_version, conf):
        self.plat_bit = ''.join([plat, browser_bit])
        self.version = driver_version
        self.conf = conf.items(self.__class__.__name__)
        self.url = self.conf.get('url').format(self.version, self.down_file())

    def down_file(self):
        ''' 下载的 webdriver 文件'''
        return self.conf.get(''.join(['dl_', self.plat_bit]))

    def exec_file(self):
        ''' 可执行的 webdriver 文件'''
        return self.conf.get(self.plat_bit)

    def stdout(self, driver_path):
        return ''

    def latest_version(self):
        raise NotImplementedError("Please implement this method")

    def validate_response(self, resp):
        if resp.status_code == 404:
            raise ValueError(
                "No such {0} driver with version {1} by url: {2}".format(
                        self.__class__.__name__, self.version, self.url))
        elif resp.status_code != 200:
            raise ValueError(resp.json())

    def downloads(self, driver_path):
        save = File.join(driver_path, self.down_file())
        log.info('Download the {0} driver file, urlopen url : {1}'.format(
            self.__class__.__name__, self.url))
        try:
            resp = requests.get(self.url, stream=True)
            if resp.status_code == 404:
                raise ValueError(
                    "No such {0} driver with version {1} by url: {2}".format(
                        self.__class__.__name__, self.version, self.url))
        except requests.ConnectionError:
            raise requests.ConnectionError
        else:
            with open(save, 'wb') as local:
                local.write(resp.content)
            log.info('Download the {} driver Complete!'.format(self.__class__.__name__))

            kind = filetype.guess(save)
            if kind is None:
                raise exceptions.FileTypeError(save)
            cls = getattr(uncompress, kind.extension)
            obj = cls(save, driver_path)
            obj.extract()

    def remove_driver(self, driver_path):
        File.remove(File.join(driver_path, self.exec_file()))

    def move_driver(self, driver_path):
        pass


class chrome(Driver):

    def __init__(self, plat, browser_bit, driver_version, conf):
        """
        :type plat: str
        """
        super(chrome, self).__init__(plat, browser_bit, driver_version, conf)

    def stdout(self, driver_path):
        dr = os.popen(u'{0}/{1} -v'.format(driver_path, self.exec_file())).read().strip()
        return dr.split(" ")[1]

    def latest_version(self):
        return requests.get(self.conf.get('latest_url')).text.strip()

class firefox(Driver):

    def __init__(self, plat, browser_bit, driver_version, conf):
        super(firefox, self).__init__(plat, browser_bit, driver_version, conf)

    def down_file(self):
        if self.conf.get(''.join(['dl_', self.plat_bit])):
            return self.conf.get(''.join(['dl_', self.plat_bit])).format(self.version)

    def stdout(self, driver_path):
        dr = os.popen('{0}/{1} -V'.format(driver_path, self.exec_file())).read().strip()
        return dr.split('\n')[0].split(' ')[1]

    def latest_version(self):
        resp = requests.get(self.conf.get('latest_url'))
        self.validate_response(resp)
        return resp.json()["tag_name"][1:]
