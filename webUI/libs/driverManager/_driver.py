#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  driverManager.py [Python]
# @Version    :  python3.6
# @Author     :  mitool
# @Date       :  2018/11/27
# @IDE        :  PyCharm
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------

import os

from webUI.libs.common.fileHelper import File
from webUI.libs.common.logHelper import logger


log = logger(__name__)


class Driver(object):

    def __init__(self, plat, browser_bit, driver_version, conf):
        self.plat = plat
        self.bit = browser_bit
        self.version = driver_version
        self.conf = conf.items(self.__class__.__name__)
        self.url = self.conf.get('url').format(self.version, self.dl_filename)

    def dl_filename(self):
        return self.conf.get(''.join(['dl_', self.plat, self.bit]))

    def exec_file(self):
        return self.conf.get(''.join([self.plat, self.bit]))

    def stdout(self, driver_path):
        return ''

    def latest_version(self):
        # type: () -> str
        raise NotImplementedError("Please implement this method")

    def validate_response(self, resp):
        if resp.status_code == 404:
            raise ValueError(
                "no such {0} driverManager with version : {1}".format(
                    self.__class__.__name__, self.version))
        elif resp.status_code != 200:
            raise ValueError(resp.json())

    def __request(self, driver_path):
        save = File.join(driver_path, self.dl_filename)
        log.info('Downloading the {0} driver file, urlopen url : {1}'.format(
            self.__class__.__name__, self.url))
        try:
            resp = requests.get(self.url, stream=True)
            if resp.status_code == 404:
                raise ValueError(
                    "No such {0} driver with version {1} by url: {2}".format(
                        self.__class__.__name__, self.version, self.url))
        except requests.ConnectionError:
            raise requests.ConnectionError
        else:
            with open(save, 'wb') as local:
                local.write(resp.content)
            log.info('Download the {} driver Complete!'.format(self.__class__.__name__))

    def install(self, driver_path):
        driver = File.join(driver_path, self.exec_file())
        if not File.isfile(driver):
            self.__request(driver_path)
            self.move_driver(driver_path)
        else:
            # todo: check mac,linux
            st = self.stdout(driver_path)
            log.info('Exist driverManager : {0}'.format(st))
            if self.version not in st:
                log.info('Need {0} driverManager[version:{1}], remove and download.'.format(
                    self.__class__.__name__, self.version))
                self.remove_driver(driver_path)
                self.__request(driver_path)
                self.move_driver(driver_path)
            else:
                log.info('Need {0} driverManager[version:{1}], exist the driverManager.'.format(
                    self.__class__.__name__, self.version))

    def remove_driver(self, driver_path):
        File.remove(File.join(driver_path, self.exec_file()))

    def move_driver(self, driver_path):
        pass


class chrome(Driver):

    def __init__(self, plat, browser_bit, driver_version, conf):
        """
        :type plat: str
        """
        super(chrome, self).__init__(plat, browser_bit, driver_version, conf)

    def stdout(self, driver_path):
        return os.popen('{0}/{1} -v'.format(driver_path, self.exec_file())).read().strip()

    def latest_version(self):
        return requests.get(self.conf.get('latest_url')).text.strip()


class firefox(Driver):

    def __init__(self, plat, browser_bit, driver_version, conf):
        super(firefox, self).__init__(plat, browser_bit, driver_version, conf)

    def dl_filename(self):
        if self.conf.get(''.join(['dl_', self.plat, self.bit])):
            return self.conf.get(''.join(['dl_', self.plat, self.bit])).format(self.version)

    def stdout(self, driver_path):
        return os.popen('{0}/{1} -V'.format(driver_path, self.exec_file())).read().strip()

    def latest_version(self):
        resp = requests.get(self.conf.get('latest_url'))
        self.validate_response(resp)
        return resp.json()["tag_name"][1:]


class edge(Driver):

    def __init__(self, plat, browser_bit, driver_version, conf):
        super(edge, self).__init__(plat, browser_bit, driver_version, conf)
        if 'windows' != self.plat:
            raise Exception("Sorry! {0} driverManager only for os: windows.".format(
                self.__class__.__name__))

    def latest_version(self):
        return self.version


class ie(Driver):

    def __init__(self, plat, browser_bit, driver_version, conf):
        super(ie, self).__init__(plat, browser_bit, driver_version, conf)
        if 'windows' != self.plat:
            raise Exception("Sorry! {0} driverManager only for os: windows.".format(
                self.__class__.__name__))

    @property
    def dl_filename(self):
        if self.conf.get(''.join(['dl_', self.plat, self.bit])):
            return self.conf.get(''.join(['dl_', self.plat, self.bit])).format(self.version)

    def latest_version(self):
        return self.version


class opera(Driver):

    def __init__(self, plat, browser_bit, driver_version, conf):
        super(opera, self).__init__(plat, browser_bit, driver_version, conf)

    def stdout(self, driver_path):
        return os.popen('{0}/{1} -v'.format(driver_path, self.exec_file())).read().strip()

    def latest_version(self):
        resp = requests.get(self.conf.get('latest_url'))
        self.validate_response(resp)
        return resp.json()["name"]

    def remove_driver(self, driver_path):
        File.remove(File.join(driver_path, self.exec_file()))
        File.remove(File.join(driver_path, 'sha512_sum'))

    def move_driver(self, driver_path):
        try:
            for dirc in File.listdir(driver_path):
                if 'opera' in dirc:
                    File.move(File.join(driver_path, dirc, self.exec_file()), driver_path)
                    File.move(File.join(driver_path, dirc, 'sha512_sum'), driver_path)
                    File.rmdir(File.join(driver_path, dirc))
        except Exception:
            raise Exception('Error move opera driver.')

