#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  manager.py [Python]
# @Version    :  PY2
# @Author     :  mitool
# @Date       :  2019/4/18
# @IDE        :  PyCharm
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------

from webUI.libs.driverManager import driver
from webUI.libs.common.fileHelper import File
from webUI.libs.common.logHelper import logger

from webUI.settings import OS, browser_bit, driver_version, driverManager, browser, driver_path,allowed_download

log = logger(__name__)


def manager(driver_path):
    log.info("settings - [browser : {}], [driver_version : {}], [download_driver : {}]".format(
        browser, driver_version, allowed_download))
    if allowed_download:
        cls = getattr(driver, browser)
        obj = cls(OS, str(browser_bit), driver_version, driverManager)
        if File.isfile(File.join(driver_path, obj.exec_file())):
            _ver  = obj.stdout(driver_path)
            log.info("driver_path exists {} driver, version: {}.".format(browser, _ver))
            if driver_version in _ver:
                return
            obj.remove_driver(driver_path)
        log.info('{} driver latest version: {}, downloads version : {}'.format(
            browser, obj.latest_version(), driver_version))
        obj.downloads(driver_path)

if __name__ == '__main__':
    manager(driver_path)

