#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  settings.py [Python]
# @Version    :  PY2
# @Author     :  mitool
# @Date       :  2019/4/13
# @IDE        :  PyCharm
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------

import os
import platform

from webUI.libs.common.iniHelper import ini
from webUI.thirdParties.webdriver.X86 import linux, drawin, windows
from webUI.thirdParties.webdriver.X64 import drawin as drawin_64, windows as windows_64, linux as linux_64


# system info
OS                              = platform.system().lower()

# python version
PYTHON_VERSION                  = int(platform.python_version_tuple()[0])

'''path settings'''
ROOT_PATH                       = os.path.dirname(os.path.realpath(__file__))
CONF_PATH                       = os.path.join(ROOT_PATH, "config")
THIRDPARTIES_PATH               = os.path.join(ROOT_PATH, "thirdParties")

'''import settings'''
conf                            = ini(os.path.join(CONF_PATH, 'config.ini'))
browserMap                      = ini(os.path.join(CONF_PATH, 'browserMap.ini'))
driverVersion                   = ini(os.path.join(CONF_PATH, 'driverVersion.ini'))
driverManager                   = ini(os.path.join(CONF_PATH, 'driverManager.ini'))


''' general setting from config.ini '''
TOKEN                           = conf.get('general', 'token')
TIMEOUT                         = conf.get('general', 'timeout')
IMPLICIT_WAIT                   = conf.get('general', 'selenium_implicit_wait')

''' browsers setting from config.ini '''
_key                            = conf.get('browser', 'type').lower()
browser                         = browserMap.get('browser', _key) if _key in browserMap.options('browser') else None
browser_version                 = conf.get('browser', 'version')
browser_bit                     = conf.get('browser', 'bit')
allowed_browser                 = browser in ["chrome", "edge", "firefox", "ie", "opera", "phantomjs", "safari"]

'''  driver path setting '''
path_map                        = {'32':{'windows':os.path.dirname(os.path.realpath(windows.__file__)),
                                        'drawin':os.path.dirname(os.path.realpath(drawin.__file__)),
                                        'linux':os.path.dirname(os.path.realpath(linux.__file__))},
                                   '64':{'windows':os.path.dirname(os.path.realpath(windows_64.__file__)),
                                        'drawin':os.path.dirname(os.path.realpath(drawin_64.__file__)),
                                        'linux':os.path.dirname(os.path.realpath(linux_64.__file__))}
                                   }
driver_path                     = path_map.get(str(browser_bit)).get(OS)

''' driver setting '''
_version                        = conf.get('driver', 'version')
driver_version                  = _version if _version != '' else driverVersion.get(browser, str(browser_version))
allowed_download                = conf.get('driver', 'allowed_download')
cumstom_driver                  = conf.get('driver', 'custom_driver')


''' proxy setting'''
proxy_list                      = conf.items('proxylist')

''' download file setting for browser'''
file_save_path                  = os.path.join(ROOT_PATH, conf.get('files', 'download_dir'))
backup_path                     = os.path.join(ROOT_PATH, conf.get('files', 'backup_dir'))
is_backup                       = conf.get('files', 'is_backup')


# log setting
log_path                        = os.path.join(ROOT_PATH, conf.get('log', 'path'))
log_level                       = conf.get('log', 'level')



