#!/usr/bin/env python
# -*- coding: utf-8 -*-

# @File    : excelUtil.py
# @Version : 
# @Author  : Administrator
# @Time    : 2018/1/2 16:49
# @Software: PyCharm
# @Desc    :

import sys

reload(sys)
sys.setdefaultencoding('utf-8')

# ---------CODE BEGIN----------

import sys
import os
import natsort
from operator import itemgetter
from datetime import datetime,timedelta
from xlrd import open_workbook, cellname
from xlwt import easyxf, Workbook, Style
from xlutils.copy import copy


class xlsUtil(object):
    def __init__(self):
        self.wb = None
        self.rb = None
        self.fileName = None
        self.SheetNames = None


    def open_excel(self,filename,tempDir = 'D:\\Econ\\'):
        """
        打开excel文件
        :param   filename  ： excel 文件名称，如：filename = 'TestFile.xls'
        :param   tempDir   ： excel 文件路径，默认：tempDir = 'E:\\Src\\ExcelFile\\'
        """

        try:
            if filename.find(':')==1:
                self.rb = open_workbook(filename, formatting_info=False, on_demand=True)
            else:
                filename = os.path.join(tempDir,filename)
                print '【Opening ExcelFile】： %s' % filename
                self.rb = open_workbook(os.path.join(tempDir,filename), formatting_info=False, on_demand=True)

            self.sheetNames = self.rb.sheet_names()
            self.fileName = filename
        except Exception,e:
            print str(e)

    def get_sheet_names(self):
        '''
        获取excel 文件所有 sheet 的 名称
            return ['sheetName1','sheetName2',....]
        '''
        SheetNames = self.rb.sheet_names()
        return SheetNames

    def get_number_of_sheets(self):
        '''
        获取excel 文件所有sheet 的 数量
            return int值
        '''
        sheetNum = self.rb.nsheets
        return sheetNum

    def by_sheet(self, sheetName):
        if isinstance(sheetName,int):
            return  self.rb.sheet_by_index[sheetName]
        if isinstance(sheetName,basestring):
            return self.rb.sheet_by_name(sheetName)


    def get_column_count(self,SheetName):
        """
        获取 excel 文件中某个Sheet 的列的数量
            SheetName：sheet名称， 如：SheetName = 'Sheet1'
            return int值
        """
        sheet = self.by_sheet(SheetName)
        return sheet.ncols

    def get_row_count(self,SheetName):
        """
        获取 excel 文件中某个Sheet 的行的数量
            SheetName：sheet名称， 如：SheetName = 'Sheet1'
            return int值
        """
        sheet = self.by_sheet(SheetName)
        return sheet.nrows

    def get_column_values(self,SheetName,column,includeEmptyCells=True):
        """
         获取 excel 文件中某个Sheet 的某一列的值
             SheetName：sheet名称， 如：SheetName = 'Sheet1'
             column：int值，如：0,1,2 代表第一列，第二列...
             return [(A1,value1),(A2,value2),(A3,value3)....],A代表第一列，B代表第二列
         """
        sheet = self.by_sheet(SheetName)
        data = {}
        for row_index in xrange(sheet.nrows):
            cell = cellname(row_index,int(column))
            value = sheet.cell(row_index,int(column)).value
            data[cell] = value
        if includeEmptyCells is True:
            sortedData = natsort.natsorted(data.items(),key=itemgetter(0))
            return sortedData
        else:
            data = dict([(k,v) for (k,v) in data.items() if v])
            OrderedData = natsort.natsorted(data.items(),key=itemgetter(0))
            return OrderedData

    def get_row_values(self,SheetName,row,includeEmptyCells=True):
        """
         获取 excel 文件中某个Sheet 的某一行的值
             SheetName：sheet名称， 如：SheetName = 'Sheet1'
             row：int值，如：0,1,2 代表第一行，第二行...
             return [(A1,value1),(B1,value2),(C1,value3)....],A1,B1..后面的数字代表第一行
         """
        sheet = self.by_sheet(SheetName)
        data = {}
        for col_index in range(sheet.ncols):
            cell = cellname(int(row),col_index)
            value = sheet.cell(int(row),col_index).value
            data[cell] = value
        if includeEmptyCells is True:
            sortedData = natsort.natsorted(data.items(),key=itemgetter(0))
            return sortedData
        else:
            data = dict([(k,v) for (k,v) in data.items() if v])
            OrderedData = natsort.natsorted(data.items(),key=itemgetter(0))
            return OrderedData

    def get_sheet_values(self,SheetName,includeEmptyCells=True):
        """
         获取 excel 文件中某个Sheet 所有的值
             SheetName：sheet名称， 如：SheetName = 'Sheet1'
             return [(A1,value1),(A2,value2),(A3,value3)....]         """

        sheet = self.by_sheet(SheetName)
        data = {}
        for row_index in xrange(sheet.nrows):
            for col_index in range(sheet.ncols):
                cell = cellname(row_index,col_index)
                value = sheet.cell(row_index,col_index).value
                data[cell] = value
        if includeEmptyCells is True:
            sortedData = natsort.natsorted(data.items(),key=itemgetter(0))
            return sortedData
        else:
            data = dict([(k,v) for (k,v) in data.items() if v])
            OrderedData = natsort.natsorted(data.items(),key=itemgetter(0))
            return OrderedData

    def get_workbook_value(self,includeEmptyCells=True):
        sheetData = []
        workbookData = []
        for sheet_name in self.SheetNames:
            if includeEmptyCells is True:
                sheetData = self.get_sheet_values(sheet_name)
            else:
                sheetData = self.get_sheet_values(sheet_name,False)
            sheetData.insert(0,sheet_name)
            workbookData.append(sheetData)
        return workbookData

    def get_cell_data_by_name(self,SheetName,cell_name):

        # Uses the cell name to return the data from that cell.

        sheet = self.rb.sheet_by_name(SheetName)
        for row_index in xrange(sheet.nrows):
            for col_index in range(sheet.ncols):
                cell = cellname(row_index,col_index)
                if cell_name ==cell:
                    cellValue = sheet.cell(row_index,col_index).value
        return cellValue

    def get_cell_data_by_coordinates(self, SheetName, column, row):

        # Uses the column and row to return the data from that cell.

        my_sheet_index = self.SheetNames.index(SheetName)
        sheet = self.rb.sheet_by_index(my_sheet_index)
        cellValue = sheet.cell(int(row), int(column)).value
        return cellValue

    # def set_style(self, style='font:height 240, color-index red, bold on;align: wrap on, vert centre, horiz center'):
    #     return easyxf(style)
    #
    # def write(self, row, col, value, styl=Style.default_style):
    #     pass




