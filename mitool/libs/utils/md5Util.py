#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2017/10/31 17:43
# @Author  : MITool
# @File    : md5Util.py
# @Version : 
# @Desc    : 
# @Software: PyCharm

import sys

reload(sys)
sys.setdefaultencoding('utf-8')

# ---------CODE BEGIN----------

from hashlib import md5
import json
import os


class md5Util(object):

    @staticmethod
    def md5HEX(str_):
        '''
        MD5加密算法，
        :param string: 字符串
        :return: md5
        '''
        # print type(str_)
        if isinstance(str_, unicode): str_ = str_.encode('utf-8')
        if isinstance(str_, dict): str_ = json.dumps(str_)
        if not isinstance(str_, str): str_ = str(str_)
        m = md5()
        m.update(str_.strip())
        return m.hexdigest()


    @staticmethod
    def md5File(file):
        '''
        文件MD5编码
        :param file:
        :return:
        '''
        statinfo = os.stat(file)

        if int(statinfo.st_size) / (1024 * 1024) >= 1000:
            print "File size > 1G, move to big file..."
            return md5Util.md5BigFile(file)

        with open(file, 'rb') as f:
            m = md5()
            m.update(f.read())
            return m.hexdigest()

    @staticmethod
    def md5BigFile(file, buffer=8192):
        '''
        大文件MD5编码
        :param file:
        :return:
        '''
        with open(file, 'rb') as f:
            m = md5()
            while 1:
                # why is 8192 | 8192 is fast than 2048
                chunk = f.read(buffer)
                if not chunk: break
                m.update(chunk)
                return m.hexdigest()

    @staticmethod
    def md5Folder(dir,md5File):
        '''

        :param dir: 文件路径
        :param md5File:  md5文件
        :return: 返回md5文件
        '''
        with open(md5File, 'w') as f:
            for root, subdirs, files in os.walk(dir):
                for file in files:
                    filefullpath = os.path.join(root, file)
                    filerelpath = os.path.relpath(filefullpath, dir)
                    md5 = md5Util.md5File(filefullpath)
                    f.write(filerelpath + '\t' + md5 + "\n")



if __name__ == '__main__':
    print  md5Util.md5HEX({'name':'admin'})




