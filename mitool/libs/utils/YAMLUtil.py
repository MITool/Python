#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  YAMLUtil.py [Python]
# @Version    :  python3.6
# @Author     :  mitool
# @Date       :  2018/11/7
# @IDE        :  PyCharm
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------

import os
import yaml
import codecs


class YAMLUtil(object):
    '''读取yaml配置文件'''

    def __init__(self, path):
        curPath = os.path.dirname(os.path.abspath(__file__))
        self.__path = os.path.join(curPath, path)
        if not os.path.isfile(self.__path):
            raise Exception

    def load(self):
        with codecs.open(self.__path, 'r', encoding='utf-8') as fp:
            content = fp.read()
        return yaml.load(content)

    def dump(self, data):
        if not isinstance(data, dict):
            raise TypeError("TypeError: data type not dict.")
        with codecs.open(self.__path, 'a', encoding='utf-8') as fp:
            yaml.dump(data, fp, default_flow_style=False)

