#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2017/10/19 14:00
# @Author  : MITool
# @File    : codeUtil.py
# @Version : 
# @Desc    : 
# @Software: PyCharm

import os,sys
import urllib

class codeUtil(object):

    @staticmethod
    def UTF8_TO_GBK(string):
        '''
        utf-8 code convert gbk code
        :param string:
        :return:
        '''
        if not isinstance(string, str):
            raise TypeError
        return string.decode('utf-8').encode('gbk')

    @staticmethod
    def GBK_TO_UTF8(string):
        '''
        gbk code convert utf-8 code
        :param string:
        :return:
        '''
        if not isinstance(string, str):
            raise TypeError
        return string.decode('gbk').encode('utf-8')

    @staticmethod
    def URLEncode(string):
        '''
        string convert urlcode
        :param string:
        :return:
        '''
        if not isinstance(string, str):
            raise TypeError
        return urllib.quote(str)

    @staticmethod
    def URLDecode(string):
        '''
        urlcode convert string
        :param string:
        :return:
        '''
        if not isinstance(string, str):
            raise TypeError
        return urllib.unquote(string)

