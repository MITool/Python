#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2017/11/15 17:47
# @Author  : MITool
# @File    : fileUtil.py
# @Version :
# @Desc    :
# @Software: PyCharm

import sys

reload(sys)
sys.setdefaultencoding('utf-8')

# ---------CODE BEGIN----------
import os
import shutil
import chardet

class File(object):

    def __init__(self, file=None, path=None):
        self.file = file
        self.path = path


    def clear(self, file=None ,mode='wb'):
        '''
        清空文件内容
        :param file:       文件名
        :param mode:       读写mode
        :return:           True | False
        '''
        self.file = file if file is not None else self.file
        try:
            with open(self.file, mode) as f:
                f.truncate()
            return True
        except Exception:
            return False


    def write(self, content, file=None, mode='wb'):
        '''
        写入文件内容
        :param content:      写入内容
        :param file:         文件名
        :param mode:         写mode
        :return:
        '''
        self.file = file if file is not None else self.file
        try:
            with open(self.file, mode) as f:
                f.write(content)
            return True
        except Exception:
            return False


    def _rep(self, content):
        '''
        清除文本 \t \r \n 信息
        :param content:  文本内容
        :return:
        '''
        for t in ['\t','\r','\n',' ']:
            content = content.replace(t,'')
        return content.strip()

    def _encode(self, content):
        '''
        文本编码
        :param content:  文本内容
        :return:
        '''
        return content.decode(chardet.detect(content)['encoding'], "ingore")

    def read(self, file=None, encoding=None, rep=False):
        '''
        读取文本内容
        :param file:         文件名
        :param encoding:     编码
        :param rep:          是否清除, True | False
        :return:             文本内容
        '''
        self.file = file if file is not None else self.file
        try:
            with open(self.file, 'r') as f:
                content = f.read()
                if encoding==None or len(encoding)<1:
                    content = self._encode(content)
                if rep:
                    return self._rep(content)
                return content
        except Exception as e:
            print e


    def readline(self, file=None, encoding=None, rep=False):
        '''
        读取文本内容
        :param file:         文件名
        :param encoding:     编码
        :param rep:          是否清除, True | False
        :return:             文本内容
        '''
        self.file = file if file is not None else self.file
        try:
            with open(self.file, 'r') as f:
                while 1:
                    content = f.readline()
                    if not content:break
                    content = self._encode(content) if encoding == None or len(encoding) < 1 else content
                    content = self._rep(content) if rep else content
                    yield content
        except Exception as e:
            print e


    def readlines(self, file=None, encoding=None, rep=False):
        '''
        读取文本内容
        :param file:         文件名
        :param encoding:     编码
        :param rep:          是否清除, True | False
        :return:             文本内容
        '''
        self.file = file if file is not None else self.file
        res = []
        try:
            with open(self.file, 'r') as f:
                for content in f.readlines():
                    content = self._encode(content) if encoding == None or len(encoding) < 1 else content
                    content = self._rep(content) if rep else content
                    res.append(content)
                return res
        except Exception as e:
            print e



    def isdir(self, path=None):
        '''
        判断是否为路径
        :param path:   路径名
        :return:       是否路径
        '''
        self.path = path if path is not None else self.path
        return os.path.isdir(self.path)

    def isfile(self, path=None):
        '''
        判断是否为文件
        :param path:   路径名
        :return:       是否文件
        '''
        self.path = path if path is not None else self.path
        return os.path.isfile(self.path)

    def exists(self, path=None):
        '''
        判断文件是否存在
        :param path:  路径名
        :return:      （bool）是否存在
        '''
        self.path = path if path is not None else self.path
        return os.path.exists(self.path)


    def mkdir(self, path=None):
        '''
        建立单级目录，返回是否成功
        1、如果存在且是目录则返回True
        2、如果存在，但是文件不是目录则返回False
        3、如果不存在则建立目录，返回成功与否
        :param path:  (str) 目录路径 -> D:/123
        :return: 建立单级目录是否成功
        '''
        self.path = path if path is not None else self.path
        if self.isdir(self.path):
            return True
        if self.isfile(self.path):
            return False
        try:
            os.mkdir(self.path)
            return True
        except Exception as e:
            return False

    def mkdirs(self, path=None):
        '''
        建立多级目录，返回是否成功
        1、如果存在且是目录则返回True
        2、如果存在，但是文件不是目录则返回False
        3、如果不存在则建立多级目录，返回成功与否
        :param path:  (str) 目录路径 -> D:/123
        :return: 建立目录是否成功

        '''
        self.path = path if path is not None else self.path
        if self.isdir(self.path):
            return True
        if self.isfile(self.path):
            return False
        try:
            os.makedirs(self.path)
            return True
        except Exception as e:
            return False


    def copyfile(self, src, aim):
        '''
        复制单个文件，返回是否成功
        :param src: (unicode) 源文件名
        :param aim: (unicode) 目标文件名
        :return: (bool) ->True：复制成功,False:复制失败
        '''
        try:
            shutil.copyfile(src, aim)
            return True
        except Exception as e:
            return False

    def copytree(self, src, aim):
        '''
        复制目录树，返回是否成功
        :param src: (unicode) 源文件名
        :param aim: (unicode) 目标文件名
        :return: (bool) ->True：复制成功,False:复制失败
        '''
        try:
            shutil.copytree(src, aim)
            return True
        except Exception as e:
            return False


    def rname(self, old, new):
        """
        重命名
        :param old: （unicode) 源文件名
        :param new:  (unicode) 目标文件名
        :return: (bool) 是否成功 -> True:成功,False:失败
        """
        try:
            os.rename(old, new)
            return True
        except Exception as e:
            return False

    def rmove(self, file=None):
        """
        删除文件
        :param file:  (str) 文件名
        :return: （bool) 是否成功 -> True：成功 False:失败
        """
        self.file = file if file is not None else self.file
        if self.exists(self.file):
            try:
                os.remove(self.file)
                return True
            except Exception as e:
                return False
        else:
            return True

    def rmdir(self, path=None):
        """
        删除目录或文件
        :param dir: (str)  目录名
        :return: (bool) 是否成功 -> True:成功 False:失败
        """
        self.path = path if path is not None else self.path
        try:
            shutil.rmtree(self.path)
            return True
        except Exception as e:
            return False


    def join(self, file=None, path=None):
        '''
        拼接文件绝对路径
        :param file:   文件名
        :param path:   路径名
        :return:
        '''
        self.path = path if path is not None else self.path
        self.file = file if file is not None else self.file
        return os.path.join(self.path, self.file)

    def getsize(self, file=None):
        '''
        获取文件大小字节数
        :param file:   文件名
        :return:       （long）文件字节数
        '''
        self.file = file if file is not None else self.file
        return os.path.getsize(self.file)





