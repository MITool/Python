#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2017/10/20 11:35
# @Author  : MITool
# @File    : charUtil.py
# @Version : 
# @Desc    : 
# @Software: PyCharm

import sys

reload(sys)
sys.setdefaultencoding('utf-8')

# ---------CODE BEGIN----------

import re

class charUtil(object):

    @staticmethod
    def allNumeric(ustring):
        '''
        return True if string all is numeric
        :param s:
        :return:
        '''
        return all(c in u"0123456789.+-" for c in ustring)

    @staticmethod
    def inspechar(SPE,ustring):
        '''
        return True if string include any spcical char
        :param ustr:
        :return:
        '''
        return any(c in SPE for c in ustring)

    @staticmethod
    def  isChinese(uchar):
        """
        判断一个unicode是否是汉字
        :param uchar:
        :return:
        """
        if  uchar >= u'\u4e00'  and  uchar<=u'\u9fa5' :
            return True
        return False

    @staticmethod
    def allChinese(ustring):
        '''
        returns True if string s is chinese
        :param strs:
        :return:
        '''
        return all(charUtil.isChinese(c)==True for c in ustring)

    @staticmethod
    def  isNumber(uchar):
        """
        判断一个unicode是否是数字
        :param uchar:
        :return:
        """
        if  uchar >= u'\u0030' and  uchar<=u'\u0039' :
            return  True
        return  False

    @staticmethod
    def  isAlphabet(uchar):
        """
        判断一个unicode是否是英文字母
        :param uchar:
        :return:
        """
        if  (uchar >= u'\u0041' and uchar<=u'\u005a') or (uchar >= u'\u0061' and uchar <= u'\u007a'):
            return  True
        return  False
    @staticmethod
    def  isOther(uchar):
        """
         判断是否非汉字，数字和英文字符
        :param uchar:
        :return:
        """
        if not (charUtil.isChinese(uchar) or
                    charUtil.isNumber(uchar) or
                    charUtil.isAlphabet(uchar)):
            return  True
        return  False

    @staticmethod
    def inOther(ustring):
        '''
        return Ture if string ustr include any char exception chinese ,number and english
        :param ustr:
        :return:
        '''
        return any(charUtil.isOther(s) == True for s in ustring)

    @staticmethod
    def  B2Q(uchar):
        """
        半角转全角
        :param uchar:
        :return:
        """
        inside_code=ord(uchar)
        if  inside_code<0x0020 or inside_code>0x7e:       # 不是半角字符就返回原来的字符
            return  uchar
        if  inside_code==0x0020:  # 除了空格其他的全角半角的公式为:半角=全角-0xfee0
            inside_code=0x3000
        else :
            inside_code+=0xfee0
        return  unichr(inside_code)

    @staticmethod
    def  stringB2Q(ustring):
        """
        将字符串中半角转全角
        :param ustring:
        :return:
        """
        return   "" .join([charUtil.B2Q(uchar)  for  uchar  in  ustring])

    @staticmethod
    def  Q2B(uchar):
        """
        全角转半角
        :param uchar:
        :return:
        """
        inside_code=ord(uchar)
        if  inside_code==0x3000:
            inside_code=0x0020
        else :
            inside_code-=0xfee0
        if  inside_code<0x0020  or  inside_code>0x7e:       # 转完之后不是半角字符返回原来的字符
            return  uchar
        return  unichr(inside_code)

    @staticmethod
    def  stringQ2B(ustring):
        """将字符串中全角转半角"""
        return   "" .join([charUtil.Q2B(uchar)  for  uchar  in  ustring])

    @staticmethod
    def  uniform(ustring):
        """
        格式化字符串，完成全角转半角，大写转小写的工作
        :param ustring:
        :return:
        """
        return  charUtil.stringQ2B(ustring).lower()

    @staticmethod
    def  string2List(ustring):
        """
         将ustring按照中文，字母，数字分开
        :param ustring:
        :return:
        """
        retList=[]
        utmp=[]
        for  uchar  in  ustring:
            if  charUtil.isOther(uchar):
                if  len(utmp)==0:
                    continue
                else :
                    retList.append( "" .join(utmp))
                    utmp=[]
            else :
                utmp.append(uchar)
        if  len(utmp)!=0:
            retList.append( "" .join(utmp))
        return  retList




