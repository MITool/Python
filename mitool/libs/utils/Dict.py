#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  Dict.py [Python]
# @Version    :  python3.6
# @Author     :  mitool
# @Date       :  2018/11/8
# @IDE        :  PyCharm
# ---------------------------------------------------
'''
    Description:
         
'''

# ---------------------------------------------------

from operator import itemgetter


class Dict(dict):

    def __init__(self):
        super(Dict, self).__init__()

    '''
        通过使用__setattr__,__getattr__,__delattr__
        可以重写dict,使之通过“.”调用
    '''

    def __setattr__(self, key, value):
        self[key] = value

    def __getattr__(self, key):
        try:
            return self[key]
        except KeyError:
            return KeyError

    def __delattr__(self, key):
        try:
            del self[key]
        except KeyError:
            return KeyError

    # __call__方法用于实例自身的调用,达到()调用的效果
    def __call__(self, key):  # 带参数key的__call__方法
        try:
            return self[key]
        except KeyError:
            return KeyError

    def sort(self, obj, key=0, reverse=False):
        '''
        :param reverse: False,升序 True 降序，默认False
        :return:
        '''
        if not isinstance(obj, dict):
            raise TypeError
        if int(key) not in [0, 1]:
            raise ValueError("key must in [0, 1], 0 : sort by key, 1: sort by value.")
        return sorted(obj.iteritems(), key=itemgetter(int(key)), reverse=reverse)

    def swap(self, obj):
        ''' k-v swap'''
        if not isinstance(obj, dict):
            raise TypeError
        return dict(zip(obj.values(), obj.keys()))

    def join(self, obj, dic):
        if not (isinstance(obj, dict) and
                isinstance(dic, dict)):
            raise TypeError
        temp = dict()
        for k, v in obj.iteritems():
            temp.setdefault(k, set()).add(v)
        for k, v in dic.iteritems():
            temp.setdefault(k, set()).add(v)
        return temp

    # 字典根据value排序
    # >> > sorted(mydict.iteritems(), key=lambda x: x[1])
    # [('a', 10), ('c', 10), ('b', 10), ('d', 20)]
    # 1
    # 2
    # 字典先按key排序，然后按value排序
    # >> > sorted(mydict.iteritems(), key=lambda x: (x[0], x[1]))
    # [('a', 10), ('b', 10), ('c', 10), ('d', 20)]
    # 1
    # 2
    # 字典先按value排序，然后按key排序
    # >> > sorted(mydict.iteritems(), key=lambda x: (x[1], x[0]))
    # [('a', 10), ('b', 10), ('c', 10), ('d', 20)]
    # 1
    # 2
    # 字典先按value升序排序，然后按key降序排序
    # >> > sorted(mydict.iteritems(), key=lambda x: (x[1], -ord(x[0])))
    # [('c', 10), ('b', 10), ('a', 10), ('d', 20)]
    # 1
    # 2
    # 字典先按value降序排序，然后按key升序排序
    # >> > sorted(mydict.iteritems(), key=lambda x: (x[1], -ord(x[0])), reverse=True)
    # [('d', 20), ('a', 10), ('b', 10), ('c', 10)]