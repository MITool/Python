#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  logHelper.py [Python]
# @Version    :  python3.6
# @Author     :  mitool
# @Date       :  2018/11/5
# @IDE        :  PyCharm
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------

import logging
import logging.config
import os
import datetime

def get_logger():
    BASE_DIR = os.path.dirname(os.path.abspath(__file__))
    LOG_DIR = os.path.join(BASE_DIR, '../logs')
    # 创建路径
    if not os.path.exists(LOG_DIR):
        os.makedirs(LOG_DIR)

    LOG_FILE =  datetime.datetime.now().strftime("%Y%m%d") + ".log"

    LOG_CONF = {
        "version": 1,
        "disable_existing_loggers": False,
        "formatters": {
            "simple": {
                '()': 'colorlog.ColoredFormatter',
                'format': '%(log_color)s%(asctime)s - %(filename)s[line:%(lineno)4d] - %(levelname)-8s %(blue)s : %(message)s',
                'log_colors': {
                    'DEBUG': 'cyan',
                    'INFO': 'green',
                    'WARNING': 'yellow',
                    'ERROR': 'red',
                    'CRITICAL': 'red',
                }
            },
            'standard': {
                'format': '%(asctime)s - %(filename)s[line:%(lineno)4d] - %(levelname)-8s : %(message)s'
            },
        },

        "handlers": {
            "console": {
                "class": "colorlog.StreamHandler",
                "level": "INFO",
                "formatter": "simple",
                "stream": "ext://sys.stdout"
            },
            "default": {
                "class": "logging.handlers.RotatingFileHandler",
                "level": "INFO",
                "formatter": "standard",
                "filename": os.path.join(LOG_DIR, LOG_FILE),
                'mode': 'w+',
                "maxBytes": 1024 * 1024 * 50,  # 50 MB
                "backupCount": 20,
                "encoding": "utf8"
            },
            "error_file_handler": {
                "class": "logging.handlers.RotatingFileHandler",
                "level": "ERROR",
                "formatter": "standard",
                "filename": os.path.join(LOG_DIR, "errors.log"),
                "maxBytes": 1024 * 1024 * 10,  # 10 MB
                "backupCount": 20,
                "encoding": "utf8"
            }
        },
        "root": {
            'handlers': ['default', 'console', 'error_file_handler'],
            'level': "INFO",
            'propagate': False
        }
    }
    try:
        logging.config.dictConfig(LOG_CONF)
    except ValueError:
        LOG_CONF['formatters']['simple'] = {'format': '%(asctime)s - %(filename)s[line:%(lineno)4d] - %(levelname)-8s : %(message)s'}
        LOG_CONF['handlers']['console']['class'] = "logging.StreamHandler"
        logging.config.dictConfig(LOG_CONF)
    return logging.getLogger(__file__)

logger = get_logger()

# logging.basicConfig(level=logging.DEBUG,
#                     format='%(asctime)s - %(filename)s[line:%(lineno)d] - %(levelname)-8s : %(message)s')  # logging.basicConfig函数对日志的输出格式及方式做相关配置
# # 由于日志基本配置中级别设置为DEBUG，所以一下打印信息将会全部显示在控制台上
logging.info('this is a loggging info message')
logging.debug('this is a loggging debug message')
logging.warning('this is loggging a warning message')
logging.error('this is an loggging error message')
logging.critical('this is a loggging critical message')