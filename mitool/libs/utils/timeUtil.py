#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2017/10/18 16:49
# @Author  : MITool
# @Site    : 
# @File    : timeUtil.py
# @Software: PyCharm


import sys

reload(sys)
sys.setdefaultencoding('utf-8')

import time
from charUtil import charUtil
from datetime import datetime
from dateutil.relativedelta import relativedelta
from dateutil.parser import parse
from dateutil.rrule import rrule,DAILY,SECONDLY



class timeUtil(object):

    @staticmethod
    def timeStamp():
        '''
        返回当前时间戳
        :return: float
        '''
        return time.time()

    @staticmethod
    def currentDate(format=u"%Y-%m-%d"):
        '''
        返回当前日期
        :return: str
        '''
        return timeUtil.convert(time.localtime(),format)


    @staticmethod
    def stringToStamp(date_str, format):
        '''
        返回给定字符串格式化后的时间戳[时间字符串 --> 时间戳]
        :param date_str:  给定字符串
        :param format:  给定格式
        :return: float
        '''
        if isinstance(date_str, basestring):
            return time.mktime(time.strptime(date_str,format))

    @staticmethod
    def convert(_time, format=None):
        '''
        返回给定格式的时间[时间戳 --> 时间字符串]，默认格式：%Y-%m-%d %H:%M:%S
        :param _time: 给定时间<timetuple>
        :param format: 给定格式
        :return: str
        '''
        if format:
            return time.strftime(format, _time)
        return time.strftime(u"%Y-%m-%d %H:%M:%S", _time)

    @staticmethod
    def currentTime(format=None):
        """
        返回给定格式的当前时间[时间戳 --> 时间字符串]，默认格式：%Y-%m-%d %H:%M:%S
        :param format: 给定格式
        :return: str
        """
        return timeUtil.convert(time.localtime(), format)

    @staticmethod
    def __stampCheck(stamp):
        '''
        时间戳 check
        :param stamp:
        :return:
        '''
        if isinstance(stamp, basestring) and charUtil.allNumeric(stamp):
            stamp = abs(long(stamp))

        if isinstance(stamp,(long, int)):
            stamp_str = str(stamp)
            if len(stamp_str) > 10:
                stamp_str = stamp_str[:10] + '.' + stamp_str[10:]
            stamp = float(stamp_str)

        if isinstance(stamp, float):
            return True,stamp
        else:return False,None

    @staticmethod
    def stampToDate(stamp, format=None):
        """
        返回给定时间戳格式化后的时间[时间戳 --> 时间字符串]，默认格式：%Y-%m-%d %H:%M:%S
        :param stamp: 时间戳
        :param format: 格式
        :return:str
        """
        tup = timeUtil.__stampCheck(stamp)
        if tup[0]:
            return timeUtil.convert(time.localtime(tup[1]), format)
        return None

    @staticmethod
    def stamp2LOCAL(stamp, format):
        '''
        返回当地时间
        :param stamp:  时间戳
        :param format:  格式
        :return:
        '''
        tup = timeUtil.__stampCheck(stamp)
        if tup[0]:
            return datetime.fromtimestamp(tup[1]).strftime(format)
        return None

    @staticmethod
    def stamp2UTC(stamp, format):
        '''
        返回UTC时间
        :param stamp:  时间戳
        :param format:  格式
        :return:
        '''
        tup = timeUtil.__stampCheck(stamp)
        if tup[0]:
            return datetime.utcfromtimestamp(tup[1]).strftime(format)
        return None

    @staticmethod
    def leapYear(year=None):
        '''
        闰年判断
        :param year: 输入的年份，未输入代表当年
        :return:True False
        '''
        if year == None:
            year = datetime.now().strftime('%Y')
        try:
            datetime(int(year),2,29)
            return True
        except ValueError:
            return False


    @staticmethod
    def timeCalc(_date=None, calcType='days', calcValue=0):
        '''
        计算时间
        :param _date:  输入的时间，未输入为当前时间
        :param calcType:  计算的类型
        :param calcValue:  计算的值
        :return:
        '''
        if _date == None:
            _date = datetime.now()
        if timeUtil.__stampCheck(_date)[0]:
            _date = timeUtil.stampToDate(_date)
        if isinstance(_date, str):
            _date = datetime.strptime(_date, '%Y-%m-%d %H:%M:%S')
        if calcType.lower() == 'years':
            return _date + relativedelta(years=calcValue)
        if calcType.lower() == 'months':
            return _date + relativedelta(months=calcValue)
        if calcType.lower() == 'days':
            return _date + relativedelta(days=calcValue)
        if calcType.lower() == 'hours':
            return _date + relativedelta(hours=calcValue)
        if calcType.lower() == 'minutes':
            return _date + relativedelta(minutes=calcValue)
        if calcType.lower() == 'seconds':
            return _date + relativedelta(seconds=calcValue)

    @staticmethod
    def timeDiff(start, end):
        '''
        计算时间差
        :param start: 开始时间
        :param end: 结束时间
        :return: dict
        '''
        if timeUtil.__stampCheck(start)[0]:
            start = timeUtil.stampToDate(start)
        if timeUtil.__stampCheck(end)[0]:
            end = timeUtil.stampToDate(end)
        days = (parse(end) - parse(start)).days
        seconds = (parse(end) - parse(start)).seconds
        hours = seconds / 3600
        minutes = (seconds - hours*3600) / 60
        seconds = (seconds - hours*3600) % 60
        return {'days':days,'hours':hours,'minutes':minutes,'seconds':seconds}

# if __name__ == '__main__':
#     cc = timeUtil()
#     print cc.stampToDate('1483978600', '%Y-%m-%d %H:%M:%S')
#     print cc.leapYear()





