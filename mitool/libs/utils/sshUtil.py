# -*- coding: utf-8 -*-  
# ---------------------------------------  
# Pargram：  
# Version：V 1.0  
# Author：erty 
# Date：2017/1/17 
# Function：  
#---------------------------------------

__version__ = 1.1

import sys

reload(sys)
sys.setdefaultencoding('utf-8')
sys.path.append("../")

import paramiko


class SSH(paramiko.SSHClient):
    '''由paramiko.SSHClient派生出的SSH类，进行了简单的封装。'''
    def __init__(self,host,port=None,user=None,pwd=None):
        paramiko.SSHClient.__init__(self)
        self.host = host
        self.port = port
        self.user = user
        self.pwd = pwd

    def __del__(self):
        self.disconnect()

    def sshConnect(self,user=None,pwd=None):
        '''
        windows客户端远程连接linux服务器
        调用sshConnect时，可以选填user和pwd，对初始化的用户名和密码进行更正
        '''
        try:
            if user is not None:self.user = user
            if pwd is not None:self.pwd = pwd
            self.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            self.load_system_host_keys()
            self.connect(self.host,self.port,self.user,self.pwd,timeout=30)
        except paramiko.SSHException as e:
            print e


    def execCmd(self,command):
        '''
        windows客户端远程执行linux服务器上命令
        '''
        stdin = self.exec_command(command)
        err = stdin[-1].readline()
        if "" != err:
            print "[ERROR]command: %s\n%s" % (command,err)
            return True, err
        else:print "[INFO]command: %s\n%s" %(command,stdin[1].read())

    def WtoL(self,localpath, remotepath):
        '''
        windows向linux服务器上传文件.
        localpath  为本地文件的绝对路径。如：D:\test.py
        remotepath 为服务器端存放上传文件的绝对路径,而不是一个目录。如：/tmp/my_file.txt
        '''
        client = paramiko.Transport((self.host,self.port))
        client.connect(username=self.user,password=self.pwd)
        sftp = paramiko.SFTPClient.from_transport(client)
        sftp.put(localpath,remotepath)
        client.close()


    def LtoW(self,localpath, remotepath):
        '''
        从linux服务器下载文件到本地
        localpath  为本地文件的绝对路径。如：D:\test.py
        remotepath 为服务器端存放上传文件的绝对路径,而不是一个目录。如：/tmp/my_file.txt
        '''
        client = paramiko.Transport((self.host,self.port))
        client.connect(username=self.user,password=self.pwd)
        sftp = paramiko.SFTPClient.from_transport(client)
        sftp.get(remotepath, localpath)
        client.close()


    def disconnect(self):
        self.close()

# if __name__ == '__main__':
#     ssh = SSH('10.10.20.187',22,'admin','3iuy3rjq')
#     ssh.sshConnect()
#     ssh.execCmd('ps -ef | grep python')


