#!/usr/bin/env python
# -*- coding: utf-8 -*-

# @File    : colorPrint.py
# @Version : 
# @Author  : Administrator
# @Time    : 2018/1/18 9:50
# @Software: PyCharm
# @Desc    :

import sys

reload(sys)
sys.setdefaultencoding('utf-8')

# ---------CODE BEGIN----------

# ------------------------------------------------
#   python终端显示彩色字符类，可以调用不同的方法
# 选择不同的颜色.使用方法看示例代码就很容易明白.
# ------------------------------------------------
#
# 显示格式: \033[显示方式;前景色;背景色m
# ------------------------------------------------
# 显示方式             说明
#   0                 终端默认设置
#   1                 高亮显示
#   4                 使用下划线
#   5                 闪烁
#   7                 反白显示
#   8                 不可见
#   22                非粗体
#   24                非下划线
#   25                非闪烁
#
#   前景色             背景色            颜色
#     30                40              黑色
#     31                41              红色
#     32                42              绿色
#     33                43              黃色
#     34                44              蓝色
#     35                45              紫红色
#     36                46              青蓝色
#     37                47              白色
# ------------------------------------------------
# class Colored(object):
#     # 显示格式: \033[显示方式;前景色;背景色m
#     __STYLE = {
#         'fore':
#         {   # 前景色
#             'black'    : 30,   #  黑色
#             'red'      : 31,   #  红色
#             'green'    : 32,   #  绿色
#             'yellow'   : 33,   #  黄色
#             'blue'     : 34,   #  蓝色
#             'purple'   : 35,   #  紫红色
#             'cyan'     : 36,   #  青蓝色
#             'white'    : 37,   #  白色
#         },
#
#         'back' :
#         {   # 背景
#             'black'     : 40,  #  黑色
#             'red'       : 41,  #  红色
#             'green'     : 42,  #  绿色
#             'yellow'    : 43,  #  黄色
#             'blue'      : 44,  #  蓝色
#             'purple'    : 45,  #  紫红色
#             'cyan'      : 46,  #  青蓝色
#             'white'     : 47,  #  白色
#         },
#
#         'mode' :
#         {   # 显示模式
#             'mormal'    : 0,   #  终端默认设置
#             'bold'      : 1,   #  高亮显示
#             'underline' : 4,   #  使用下划线
#             'blink'     : 5,   #  闪烁
#             'invert'    : 7,   #  反白显示
#             'hide'      : 8,   #  不可见
#         },
#
#         'default' :
#         {
#             'end' : 0,
#         },
#     }
#
#
#     def color(self, str, mode = '', fore = '', back = ''):
#         mode  = '%s' % self.__STYLE['mode'][mode.lower()] if self.__STYLE['mode'].has_key(mode.lower()) else ''
#         fore  = '%s' % self.__STYLE['fore'][fore.lower()] if self.__STYLE['fore'].has_key(fore.lower()) else ''
#         back  = '%s' % self.__STYLE['back'][back.lower()] if self.__STYLE['back'].has_key(back.lower()) else ''
#
#         style = ';'.join([s for s in [mode, fore, back] if s])
#         style = '\033[%sm' % style if style else ''
#         end   = '\033[%sm' % self.__STYLE['default']['end'] if style else ''
#         return '%s%s%s' % (style, str, end)


try:
    import colorama
    colorama.init()
except: colorama = None

# -----------------colorama模块的一些常量---------------------------
# Fore: BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE, RESET.
# Back: BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE, RESET.
# Style: DIM, NORMAL, BRIGHT, RESET_ALL

def colorPrint(text, fore=None, back=None, style=None):
    if colorama:
        part = []
        if fore:
            part.append(getattr(colorama.Fore, fore.upper(), None))
        if back:
            part.append(getattr(colorama.Back, back.upper(), None))
        if style:
            part.append(getattr(colorama.Style, style.upper(), None))
        part.append(text)
        part = filter(None, part)
        part.append(colorama.Fore.RESET + colorama.Back.RESET + colorama.Style.RESET_ALL)
        return ''.join(part)
    else:
        return text


# if __name__ == '__main__':
#     print colorPrint('颜色', fore='blue')


