#!/usr/bin/env python
# -*- coding: utf-8 -*-

# @File    : xlsxUtil.py
# @Version : 
# @Author  : Administrator
# @Time    : 2018/1/8 10:03
# @Software: PyCharm
# @Desc    :

import sys

reload(sys)
sys.setdefaultencoding('utf-8')

# ---------CODE BEGIN----------

import os
from openpyxl import load_workbook
from openpyxl import Workbook
from openpyxl.chart import BarChart, Series, Reference, BarChart3D
from openpyxl.styles import Color, Font, Alignment, Border, Side
from openpyxl.styles.colors import BLUE, RED, GREEN, YELLOW, BLACK


import natsort
from operator import itemgetter

class xlsxUtil(object):

    def __init__(self, filename,tempDir = 'D:\\Econ\\'):
        self.file = self.__get_file(filename, tempDir)
        self.wb = load_workbook(self.file)
        self.ws = self.wb.active


    def __get_file(self, filename, tempDir):
        try:
            if filename.find(':')==1:return filename
            else: return  os.path.join(tempDir,filename)
        except Exception as e: print e

    def get_sheet_names(self):
        '''
        获取excel 文件所有 sheet 的 名称
            return ['sheetName1','sheetName2',....]
        '''
        return self.wb.get_sheet_names()

    def get_number_of_sheets(self):
        '''
        获取excel 文件所有sheet 的 数量
            return int值
        '''
        return len(self.wb.get_sheet_names())

    def create_sheet(self, sheetname):
        '''
        在excel 文件中 添加 sheet
        '''
        if sheetname in self.get_sheet_names():
            raise ValueError("%s is exsits." % sheetname)
        self.wb.create_sheet(sheetname)
        self.wb.save(self.file)

    def get_sheet(self, sheetname):
        '''
        获取需要的sheet对象
        '''
        if isinstance(sheetname,int):
            return  self.wb.get_sheet_by_name(self.get_sheet_names()[sheetname])
        if isinstance(sheetname,basestring):
            return self.wb.get_sheet_by_name(sheetname)

    def get_column_count(self,sheetname):
        """
        获取 excel 文件中某个Sheet 的列的数量
            SheetName：sheet名称， 如：SheetName = 'Sheet1'
            return int值
        """
        sheet = self.get_sheet(sheetname)
        return sheet.max_column

    def get_row_count(self,SheetName):
        """
        获取 excel 文件中某个Sheet 的行的数量
            SheetName：sheet名称， 如：SheetName = 'Sheet1'
            return int值
        """
        sheet = self.get_sheet(SheetName)
        return sheet.max_row

    def get_column_values(self,SheetName,column,includeEmptyCells=True):
        """
         获取 excel 文件中某个Sheet 的某一列的值
             SheetName：sheet名称， 如：SheetName = 'Sheet1'
             column：int值，如：0,1,2 代表第一列，第二列...
             return [(A1,value1),(A2,value2),(A3,value3)....],A代表第一列，B代表第二列
         """
        sheet = self.get_sheet(SheetName)
        data = {}
        for cell in list(sheet.columns)[int(column)]:
            c = str(cell).split('.')[-1].split('>')[0]
            value = cell.value
            data[c] = value

        if includeEmptyCells is True:
            sortedData = natsort.natsorted(data.items(),key=itemgetter(0))
            return sortedData
        else:
            data = dict([(k,v) for (k,v) in data.items() if v])
            OrderedData = natsort.natsorted(data.items(),key=itemgetter(0))
            return OrderedData

    def get_row_values(self,SheetName,row,includeEmptyCells=True):
        """
         获取 excel 文件中某个Sheet 的某一行的值
             SheetName：sheet名称， 如：SheetName = 'Sheet1'
             row：int值，如：0,1,2 代表第一行，第二行...
             return [(A1,value1),(B1,value2),(C1,value3)....],A1,B1..后面的数字代表第一行
         """
        sheet = self.get_sheet(SheetName)
        data = {}
        for cell in list(sheet.rows)[int(row)]:
            c = str(cell).split('.')[-1].split('>')[0]
            value = cell.value
            data[c] = value
        if includeEmptyCells is True:
            sortedData = natsort.natsorted(data.items(),key=itemgetter(0))
            return sortedData
        else:
            data = dict([(k,v) for (k,v) in data.items() if v])
            OrderedData = natsort.natsorted(data.items(),key=itemgetter(0))
            return OrderedData

    def get_sheet_values(self, SheetName, includeEmptyCells=True):
        """
         获取 excel 文件中某个Sheet 所有的值
             SheetName：sheet名称， 如：SheetName = 'Sheet1'
             return [(A1,value1),(A2,value2),(A3,value3)....]
        """
        sheet = self.get_sheet(SheetName)
        data = {}
        for row in sheet.rows:
            for cell in row:
                c = str(cell).split('.')[-1].split('>')[0]
                value = cell.value
                data[c] = value
        if includeEmptyCells is True:
            sortedData = natsort.natsorted(data.items(),key=itemgetter(0))
            return sortedData
        else:
            data = dict([(k,v) for (k,v) in data.items() if v])
            OrderedData = natsort.natsorted(data.items(),key=itemgetter(0))
            return OrderedData

    def get_cell_data_by_coordinates(self, SheetName, row, column):
        '''
        Uses the column and row to return the data from that cell.
        '''
        sheet = self.get_sheet(SheetName)
        cellValue = sheet.cell(row=int(row), column=int(column)).value
        return cellValue

    def set_font(self, name=u'微软雅黑', size=10, color='BLACK', bold=True):
        if 'RED' == color.upper():
            return Font(name=name, size=size, color=RED, bold=bold)
        if 'YELLOW' == color.upper():
            return Font(name=name, size=size, color=YELLOW, bold=bold)
        if 'BLUE' == color.upper():
            return Font(name=name, size=size, color=BLUE, bold=bold)
        if 'GREEN' == color.upper():
            return Font(name=name, size=size, color=GREEN, bold=bold)
        return Font(name=name, size=size, color=BLACK, bold=bold)

    def set_align(self, horizontal='center', vertical='center'):
        return Alignment(horizontal=horizontal, vertical=vertical)

    def set_border(self, style='thin', color='000000'):
        left, right, top, bottom = [Side(style, color)]*4  #设置单元格边框属性
        return Border(left=left, right=right, top=top, bottom=bottom)   #设置单元格边框格式

    def write(self, sheetName, coord, value, font=None, align=None):
        # eg: coord:A1
        sheet = self.get_sheet(sheetName)
        sheet[coord] = value
        sheet[coord].border = self.set_border()
        sheet[coord].font = font if font is not None else self.set_font()
        sheet[coord].alignment = align if align is not None else self.set_align()
        self.wb.save(self.file)



