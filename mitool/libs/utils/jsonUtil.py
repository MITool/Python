#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2017/11/14 16:24
# @Author  : MITool
# @File    : jsonUtil.py
# @Version : 
# @Desc    : 
# @Software: PyCharm

import sys

reload(sys)
sys.setdefaultencoding('utf-8')

# ---------CODE BEGIN----------

import platform
import codecs

try:
    import json
except ImportError:
    try:
        import simplejson as json
    except ImportError:  # simplejson not installed
        pass


class jsonUtil(object):

    def __init__(self):
        pass

    def load(self, str):
        '''
        Json String --> Obj
        :param str:  Json String
        :return:
        '''
        return json.loads(str, encoding='utf-8')

    def dump(self, obj):
        '''
        Obj --> Json String
        :param obj:
        :return:
        # json.dumps 序列化时对中文默认使用的ascii编码.想输出真正的中文需要指定ensure_ascii=False
        # sort_keys 通过key排序
        '''
        return json.dumps(obj, ensure_ascii=False, sort_keys=True)

    def format(self, str):
        '''
        输出JSon数据
        :param str:
        :return:
        # json.dumps 序列化时对中文默认使用的ascii编码.想输出真正的中文需要指定ensure_ascii=False
        # sort_keys 通过key排序
        '''
        if isinstance(str, basestring):
            return json.dumps(self.load(str), indent=4, ensure_ascii=False, sort_keys=True)
        return json.dumps(str, indent=4, ensure_ascii=False, sort_keys=True)

    def read(self, file):
        '''
        Json 文件 --> Obj
        :param file: JSON 文件
        :return:
        '''
        try:
            with codecs.open(file, 'r') as f:
                if platform.system().lower() == 'windows':
                    for line in f.readlines():
                        yield self.load(line.replace('\\r', ''))
                elif platform.system().lower() == 'darwin':  # mac
                    for line in f.readlines():
                        yield self.load(line.replace('\\n', ''))
                else:
                    for line in f.readlines():
                        yield self.load(line)
        except IOError:
            raise IOError

    def write(self, file, obj, mode='a+'):
        '''
        写入json格式数据到文件中
        :param file:
        :param obj:
        :return:
        '''
        try:
            with codecs.open(file, mode) as f:
                # json.dumps 序列化时对中文默认使用的ascii编码.想输出真正的中文需要指定ensure_ascii=False
                str = json.dumps(obj, ensure_ascii=False, sort_keys=True)  # 处理完之后重新转为Json格式
                f.write(str.strip().encode('utf-8') + '\n')  # 写回到一个新的Json文件中去
        except IOError:
            raise IOError

    def extra(self, obj, *args):
        '''
        提取需要的数据
        :param obj: json 对象
        :param args: 输入提取的key
        :return:
        '''

        if not isinstance(obj, dict):
            raise TypeError
        return {key: obj[key] for key in obj.keys() if key in args}


    def update(self, obj, **kwargs):
        '''
        更新JSon数据
        :param obj:  json 对象
        :param kwargs:  修改的k-v
        :return:
        '''
        if not isinstance(obj, dict):
            raise TypeError
        for key in kwargs.keys():
            if key in obj.keys():
                obj[key] = kwargs[key]
        return obj

    def join(self, obj, dic):
        if not (isinstance(obj, dict) and isinstance(dic, dict)):
            raise TypeError
        temp = dict()
        for k, v in obj.iteritems():
            temp.setdefault(k, set()).add(v)
        for k, v in dic.iteritems():
            temp.setdefault(k, set()).add(v)
        return temp

if __name__ == '__main__':
    json = jsonUtil()

    d1 = {'user':"root"}
    d2 = {'user':["rootq","root"]}
    # print json.join(d1, d2)
