#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  1.9.py [Python]
# @Version    :  
# @Author     :  mitool
# @Date       :  2019/6/17
# @IDE        :  PyCharm
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------

'''
    在两个字典中寻找相同点
'''

# 两个字典
a = {
    'x': 1,
    'y': 2,
    'z': 3
}

b = {
    'w': 10,
    'x': 20,
    'y': 2
}

# 找到a、b共有的key
print(a.keys() & b.keys())

# 找到a中存在，b中存在的key
print(a.keys() - b.keys())

# 找到找到a、b共有的(key, value)
print(a.items() & b.items())