#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  1.2.py [Python]
# @Version    :  
# @Author     :  mitool
# @Date       :  2019/6/16
# @IDE        :  PyCharm
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------

'''
    Q: 需要从某个可迭代对象中分解出N个元素，但是这个可迭代对象的长度可能超过N，
       这会导致出现“分解的值过多(too many values to unpack)”的异常。
       
    A: *表达式解决
'''

def avg(args):
    return sum(args)/len(args)

def drop_first_last(grades):
    '''
    去掉第一个和最后一个，只对中间值做平均分统计
    :param grades:
    :return:
    '''
    first, *middle, last = grades
    return avg(middle)

def do_foo(x, y):
    print('foo', x, y)

def do_bar(s):
    print('bar', s)

if __name__ == '__main__':
    grades = (30, 40, 50, 60, 70, 80)
    print(drop_first_last(grades))

    records = ('Dave', 'dava@example', '773-555-1221', '847-555-1222')
    name, email, *phone_numbers = records
    print(name, email, phone_numbers)

    records = [
        ('foo', 1, 2),
        ('bar', 'hello'),
        ('foo', 3, 4),
    ]

    for tag, *args in records:
        if tag == 'foo':
            do_foo(*args)
        elif tag == 'bar':
            do_bar(*args)
