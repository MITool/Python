#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  1.6.py [Python]
# @Version    :  
# @Author     :  mitool
# @Date       :  2019/6/17
# @IDE        :  PyCharm
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------
'''
    一键多值字典(multidict): 一个能将键(key)映射到多个值的字典。
    
    eg:
    d = {
        'a': [1, 2, 3],
        'b': [4, 5]
    }
    
    e = {
        c : {1, 2, 3},
        d : {4, 5}
    }
    如果希望保留元素插入顺序，使用list；
    如果希望消除重复元素且不在意元素顺序，使用set
'''

from collections import defaultdict

d = defaultdict(list)

d['a'].append(1)
d['a'].append(2)
d['a'].append(3)
d['a'].append(3)
d['b'].append(4)
d['b'].append(5)

print(d)


d = defaultdict(set)

d['a'].add(1)
d['a'].add(2)
d['a'].add(3)
d['a'].add(3)
d['b'].add(4)
d['b'].add(5)

print(d)

