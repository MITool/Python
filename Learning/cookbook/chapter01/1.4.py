#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  1.4.py [Python]
# @Version    :  
# @Author     :  mitool
# @Date       :  2019/6/17
# @IDE        :  PyCharm
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------

'''
    Q: 在某个集合中找出最大或最小的N个元素
    
    A: heapq
'''

import heapq

nums = [1, 8, 2, 23, 7, -4, 18, 23, 42, 37, 2]



print(list(set(nums)))

print(heapq.nsmallest(3, nums))
print(heapq.nlargest(3, nums))

portfilio = [
    {'name':"IBM", 'shares':100, 'price':91.1},
    {'name':"APPL", 'shares':50, 'price':534.22},
    {'name':"FB", 'shares':200, 'price':21.01},
    {'name':"HPQ", 'shares':35, 'price':31.75},
    {'name':"YHOO", 'shares':45, 'price':16.31},
    {'name':"ACME", 'shares':75, 'price':155.65},
    {'name':"ZS", 'shares':85, 'price':666.1},
    {'name':"LS", 'shares':95, 'price':189.1},
    {'name':"WW", 'shares':90, 'price':191.1},
]

cheap = heapq.nsmallest(len(portfilio), portfilio, key=lambda s: s['price'])
expensive = heapq.nlargest(3, portfilio, key=lambda s: s['price'])

print(cheap)
print(expensive)

'''
    PS: 如果正在寻找最大或最小的N个元素，且同集合中元素的总数目相比，N很小，那么下面
        这些函数可以提供更好的性能。
    
    A:  1、首先在底层将数据转化为列表，且元素会以堆的顺序排列。
        2、堆最重要的特性就是heap[0]总是最小那个的元素。
        3、当所要找的元素数量相对较小时，heapq中函数nsmallest、nlargest才是最适合的。
        4、如果只是简单地想找到最小或最大的元素(N=1)时，那么用min()和max()最合适。
'''

heap = list(nums)
heapq.heapify(heap)
print(heap)

print(min(nums), max(nums))


# 简单的dict
lst = [('d', 2), ('a', 4), ('b', 3), ('c', 2)]

# 按照value排序
lst.sort(key=lambda k: k[1])
print(lst)

# 按照key排序
lst.sort(key=lambda k: k[0])
print(lst)

# 先按value排序再按key排序
lst.sort(key=lambda k: (k[1], k[0]))
print(lst)




# 复杂的dict，按照dict对象中某一个属性进行排序
lst = [{'level': 19, 'star': 36, 'time': 1},
       {'level': 20, 'star': 40, 'time': 2},
       {'level': 20, 'star': 40, 'time': 3},
       {'level': 20, 'star': 40, 'time': 4},
       {'level': 20, 'star': 40, 'time': 5},
       {'level': 18, 'star': 40, 'time': 1}]

# 需求:
# level越大越靠前;
# level相同, star越大越靠前;
# level和star相同, time越小越靠前;

# 先按time排序
lst.sort(key=lambda k: (k.get('time', 0)))

# 再按照level和star顺序
# reverse=True表示反序排列，默认正序排列
lst.sort(key=lambda k: (k.get('level', 0), k.get('star', 0)), reverse=True)



for idx, r in enumerate(lst):
    print('idx[%d]\tlevel: %d\t star: %d\t time: %d\t' % (idx, r['level'], r['star'],r['time']))

