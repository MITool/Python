#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  1.8.py [Python]
# @Version    :  
# @Author     :  mitool
# @Date       :  2019/6/17
# @IDE        :  PyCharm
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------

'''
    Q: 在字典上对数据执行各式各样的计算，比如：最小值，最大值，排序等；
    
    
    >>> d
    {'a': 5, 'c': 3, 'b': 4}
    
    >>> d.items()
    [('a', 5), ('c', 3), ('b', 4)]
    
    字典的元素是成键值对出现的，字典的排序可用sorted，用关键字key指定排序依据的值--key或者value
    
    按照值排序：
    
    #把d.items()所对应的列表的每个元祖的第二个元素（value）传到lambda函数进行排序
    >>> s=sorted(d.items(),key=lambda x:x[1])
    >>> s
    [('c', 3), ('b', 4), ('a', 5)]
    
    按照key排序：
    
    #把d.items()所对应的列表的每个元祖的第一个元素（key）传到lambda函数进行排序
    >>> s=sorted(d.items(),key=lambda x:x[0])
    >>> s
    [('a', 5), ('b', 4), ('c', 3)]
'''

prices = {
    'ACE': 45.23,
    'AAPL': 612.38,
    'IBM': 205.55,
    'HPQ': 37.20,
    'FB': 10.75
}

# 字典排序
sortValue = sorted(prices.items(), key= lambda d:d[1],reverse=False)
sortKey = sorted(prices.items())

print(sortValue)
print(sortKey)

# value 的最小值，最大值
print(min(zip(prices.values(), prices.keys())))
print(max(zip(prices.values(), prices.keys())))
