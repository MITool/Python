#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  1.3.py [Python]
# @Version    :  
# @Author     :  mitool
# @Date       :  2019/6/16
# @IDE        :  PyCharm
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------

'''
    Q: 在迭代或时其他形式的处理过程中对最后几项记录做一个有限的历史记录统计。
    
    A: collections.deque
'''

from collections import deque

def search(lines, pattern, history=5):
    previous_lines = deque(maxlen=history)
    for line in lines:
        if pattern in line:
            yield line, previous_lines
        previous_lines.append(line)

if __name__ == '__main__':
    with open('somefile.txt') as f:
        for line, prevlines in search(f, 'python', 5):
            for pline in prevlines:
                print(pline, end="")
            print(line, end="")
            print('-'*20)

