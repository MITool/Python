#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  1.7.py [Python]
# @Version    :  
# @Author     :  mitool
# @Date       :  2019/6/17
# @IDE        :  PyCharm
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------

'''
    Q : 创建一个字典，同时当对字典做迭代或序列化操作时，也能控制其中元素的顺序。
    
    A : 控制字典中元素的顺序，可以使用collections 模块中的OrderDict类。当对字典做
        迭代时，它会严格按照元素初始添加的顺序进行。
        
    PS: OrderDict 内部维护了一个双向链表，是普通字典的2倍多，如果构建一个涉及大量
    OrderDict实例的数据结构，需要做需求分析。 
'''

from collections import OrderedDict

d = OrderedDict()
d['foo'] = 1
d['bar'] = 2
d['spam'] = 3
d['grok'] = 4

for key in d : print(key, d[key])

import json
print(json.dumps(d))