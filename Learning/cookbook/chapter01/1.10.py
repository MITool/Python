#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  1.10.py [Python]
# @Version    :  
# @Author     :  mitool
# @Date       :  2019/6/17
# @IDE        :  PyCharm
# ---------------------------------------------------
'''
    Description:
         从序列中移除重复项且保持元素间顺序不变。
         如果序列中的值是可哈希（hashable）的，那么这个问题可以通过
         使用集合和生成器轻松解决。
'''
# ---------------------------------------------------

def dedupe(items, key=None):
    seen = set()
    for item in items:
        val = item if key is None else key(item)
        if val not in seen:
            yield item
            seen.add(val)

if __name__ == '__main__':
    a = [1, 5, 2, 1, 9, 1, 5, 10]
    print(list(dedupe(a)))

    a = [{'x':1, 'y':2}, {'x':1, 'y':3}, {'x':1, 'y':2}, {'x':2, 'y':4}]
    print(list(dedupe(a, key=lambda d: (d['x'], d['y']))))
    print(list(dedupe(a, key=lambda d: d['x'])))
    print(list(dedupe(a, key=lambda d: d['y'])))