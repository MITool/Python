#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  1.1.py [Python]
# @Version    :  
# @Author     :  mitool
# @Date       :  2019/6/16
# @IDE        :  PyCharm
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------

'''
    Q: 我们有一个包含N个元素的元组或序列，现在想将它分解为N个单独的变量。
'''

p = (4, 5)
x, y = p
print(x, y)

data = ['ACME', 50, 91.1, (2012, 12, 21)]
name, shares, price, date = data
print(name, shares, price, date)