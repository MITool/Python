#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  demo04.py [Python]
# @Version    :  PY2
# @Author     :  mitool
# @Date       :  2019/3/12
# @IDE        :  PyCharm
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------

from flask import Flask, url_for

app = Flask(__name__)

@app.route('/item/1/')
def item(id):
    pass

with app.test_request_context():
    print(url_for('item', id='1'))
    print(url_for('item', id='2', next='/'))

# if __name__ == '__main__':
#     app.run()
