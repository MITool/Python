from flask import Flask, render_template
from flask_script import Manager

app = Flask(__name__)

manager = Manager(app=app)

@app.route('/')
def hello_world():
    return 'Hello World!'

@app.route('/hello/')
def hello():
    return render_template('hello.html')


if __name__ == '__main__':
    # app.run()
    manager.run()