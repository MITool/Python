#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  demo01.py [Python]
# @Version    :  python3.6
# @Author     :  mitool
# @Date       :  2019/2/16
# @IDE        :  PyCharm
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------

from werkzeug.wrappers import Request, Response
from werkzeug.serving import run_simple

@Request.application
def hello(request):
    return Response("Hello World!")


if __name__ == '__main__':
    run_simple('localhost', 4000, hello)
