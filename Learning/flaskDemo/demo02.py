#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  demo02.py [Python]
# @Version    :  PY2
# @Author     :  mitool
# @Date       :  2019/2/16
# @IDE        :  PyCharm
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------

from flask import Flask

app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello World!"

@app.route('/item/<id>/')
def item(id):
    return 'Item: {}'.format(id)

if __name__ == '__main__':
    # app.run(debug=True)
    app.run('localhost', port=9000)
