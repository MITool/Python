#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  config.py [Python]
# @Version    :  PY2
# @Author     :  mitool
# @Date       :  2019/3/12
# @IDE        :  PyCharm
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------

DEBUG = True

try:
    from local_settings import *
except ImportError:
    pass
