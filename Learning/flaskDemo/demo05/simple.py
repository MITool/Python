#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  simple.py [Python]
# @Version    :  PY2
# @Author     :  mitool
# @Date       :  2019/3/12
# @IDE        :  PyCharm
# ---------------------------------------------------
from __future__ import print_function

'''
    Description:
         
'''
# ---------------------------------------------------

from flask import Flask, redirect, request, abort, url_for, make_response, render_template

app = Flask(__name__)
app.config.from_object('config')

@app.route('/people/')
def people():
    name = request.args.get('name')
    if not name:
        return redirect(url_for('login'))
    user_agent = request.headers.get("User-Agent")
    return 'Name: {0}; UA: {1}'.format(name, user_agent)


@app.route('/login/', methods=["GET", "POST"])
def login():
    if request.method == 'POST':
        user_id = request.form.get('user_id')
        return 'User: {} login.'.format(user_id)
    else:
        return 'Open Login Page.'

@app.route('/secret/')
def secret():
    abort(404)
    print('This is never executed.')

@app.errorhandler(404)
def not_found(error):
     return make_response(render_template("error.html"), 404)


if __name__ == '__main__':
    app.run(debug=app.debug)
