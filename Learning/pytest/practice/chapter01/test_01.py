#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  test_01.py [Python]
# @Version    :  
# @Author     :  mitool
# @Date       :  2019/6/20
# @IDE        :  PyCharm
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------

def test_passing():
    assert (1, 2, 3)  == (1, 2, 3)

def test_failing():
    assert (1, 2, 3) == (3, 2, 1)
    assert (1, 2, 3) == (1, 2, 1)
