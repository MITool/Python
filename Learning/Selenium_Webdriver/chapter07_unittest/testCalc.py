#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  testCalc.py [Python]
# @Version    :  
# @Author     :  mitool
# @Date       :  2019/7/4
# @IDE        :  PyCharm
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------

import unittest


class Calc(object):

    @classmethod
    def sum(cls, a, b):
        return a + b

    @classmethod
    def sub(cls, a, b):
        return a - b


class testCalc(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print('>>>>>>setUpClass<<<<<<<<')

    @classmethod
    def tearDownClass(cls):
        print(">>>>>>tearDownClass<<<<<<<<")


    def setUp(self):
        self.a = 3
        self.b = 1
        print('>>>>>>setUp<<<<<<<<')

    def tearDown(self):
        print(">>>>>>tearDown<<<<<<<<")


    def test_sum(self):
        self.assertEqual(Calc.sum(self.a, self.b), 4, 'test sum fail')

    def test_sub(self):
        # res = 3/0
        self.assertEqual(Calc.sub(self.a, self.b), 2, 'test sub fail')

if __name__ == '__main__':
    unittest.main()
