#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  Calc.py [Python]
# @Version    :  
# @Author     :  mitool
# @Date       :  2019/7/4
# @IDE        :  PyCharm
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------


class Calc(object):

    def add(self, a, b, *d):
        result = a + b
        for i in d:
            result += i
        return result


    def sub(self, a, b, *d):
        result = a - b
        for i in d:
            result -= i
        return result

    @classmethod
    def mul(cls, a, b, *d):
        result = a * b
        for i in d:
            result *= i
        return result

    @classmethod
    def div(cls, a, b, *d):
        if b != 0:
            result = a / b
        else:
            return -1
        for i in d:
            if i != 0:
                result /= i
            else:
                return -1
        return result