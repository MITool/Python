#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  testSuites.py [Python]
# @Version    :  
# @Author     :  mitool
# @Date       :  2019/7/4
# @IDE        :  PyCharm
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------

import unittest
import random

class test_Sequence_Functions(unittest.TestCase):

    def setUp(self):
        self.seq = range(10)

    def tearDown(self):
        pass

    def test_choice(self):
        element = random.choice(self.seq)
        self.assertTrue(element in self.seq)

    def test_sample(self):
        with self.assertRaises(ValueError):
            random.sample(self.seq, 20)
        for ele in random.sample(self.seq, 5):
            self.assertTrue(ele in self.seq)

class test_Dict_Value_Format_Functions(unittest.TestCase):

    def setUp(self):
        self.seq = range(10)

    def tearDown(self):
        pass

    def test_shuffle(self):
        # print(self.seq)
        random.shuffle(self.seq)
        # print(self.seq)
        self.seq.sort()
        self.assertEqual(self.seq, range(10))
        self.assertRaises(TypeError, random.shuffle, (1, 2, 3))

if __name__ == '__main__':
    testCase1 = unittest.TestLoader().loadTestsFromTestCase(test_Sequence_Functions)
    testCase2 = unittest.TestLoader().loadTestsFromTestCase(test_Dict_Value_Format_Functions)

    suite = unittest.TestSuite([testCase1, testCase2])
    unittest.TextTestRunner(verbosity=2).run(suite)
