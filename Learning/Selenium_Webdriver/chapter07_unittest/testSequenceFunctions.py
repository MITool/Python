#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  testSequenceFunctions.py [Python]
# @Version    :  
# @Author     :  mitool
# @Date       :  2019/7/4
# @IDE        :  PyCharm
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------

import unittest
import random

class test_Sequence_Functions(unittest.TestCase):

    def setUp(self):
        self.seq = range(10)

    def test_run(self):
        element = random.choice(self.seq)
        self.assertTrue(element in self.seq)

class test_Dict_Value_Format_Functions(unittest.TestCase):

    def setUp(self):
        self.seq = range(10)

    def test_shuffle(self):
        random.shuffle(self.seq)
        self.seq.sort()
        self.assertEqual(self.seq, range(10))
        self.assertRaises(TypeError, random.shuffle, (1, 2, 3))

if __name__ == '__main__':
    unittest.main()



