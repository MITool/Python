#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  myTest.py [Python]
# @Version    :  
# @Author     :  mitool
# @Date       :  2019/7/4
# @IDE        :  PyCharm
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------

from Calc import Calc
import unittest


class myTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print("[setUpClass] : 创建 Calc 实例类")
        cls.c = Calc()


    def test_add(self):
        print('[testCase] : run Calc.add()')
        self.assertEqual(myTest.c.add(1, 2, 12), 15, 'test add fail')

    def test_sub(self):
        print('[testCase] : run Calc.sub()')
        self.assertEqual(myTest.c.sub(5, 2, 3), 0, 'test sub fail')

    def test_mul(self):
        print('[testCase] : run Calc.mul()')
        self.assertEqual(myTest.c.mul(1, 2, 12), 24, 'test mul fail')

    def test_div(self):
        print('[testCase] : run Calc.div() ')
        self.assertEqual(myTest.c.div(12, 2, 3), 2, 'test div fail')

if __name__ == '__main__':
    # unittest.main()

    suite = unittest.TestSuite()
    suite.addTest(myTest('test_mul'))
    suite.addTest(myTest('test_sub'))
    suite.addTest(myTest('test_add'))
    suite.addTest(myTest('test_div'))

    runner = unittest.TextTestRunner()
    runner.run(suite)


