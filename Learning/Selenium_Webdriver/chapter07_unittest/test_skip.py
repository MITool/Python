#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  test_skip.py [Python]
# @Version    :  
# @Author     :  mitool
# @Date       :  2019/7/5
# @IDE        :  PyCharm
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------

import random
import unittest
import sys


class test_sequence_functions(unittest.TestCase):

    a = 1

    def setUp(self):
        self.seq = list(range(10))

    @unittest.skip('skipping')
    def test_shuffle(self):
        random.shuffle(self.seq)
        self.seq.sort()
        self.assertEqual(self.seq, list(range(10)))
        self.assertRaises(TypeError, random.shuffle, (1, 2, 3))

    @unittest.skipIf(a>5, 'condition is not staisfied')
    def test_choice(self):
        element = random.choice(self.seq)
        self.assertTrue(element in self.seq)


    @unittest.skipUnless(sys.platform.startswith('linux'), 'requires Windows')
    def test_sample(self):
        with self.assertRaises(ValueError):
            random.sample(self.seq, 20)
        for element in random.sample(self.seq, 5):
            self.assertTrue(element in self.seq)

if __name__ == '__main__':

    testCases = unittest.TestLoader.loadTestsFromTestCase(test_sequence_functions)
    suites = unittest.TestSuite(testCases)
    unittest.TextTestRunner(verbosity=2).run(suites)