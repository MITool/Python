#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  test_assert.py [Python]
# @Version    :  
# @Author     :  mitool
# @Date       :  2019/7/14
# @IDE        :  PyCharm
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------


import sys
reload(sys)
sys.setdefaultencoding('utf8')

import unittest
import HtmlTestRunner
import random

class myClass(object):

    @classmethod
    def sum(cls, a, b):
        return a + b

    @classmethod
    def div(cls, a, b):
        return a/b

    @classmethod
    def return_None(cls):
        return None

class myAssert(unittest.TestCase):

    def test_assertEqual(self):
        try:
            a, b = 1, 2
            sum = 13
            self.assertEqual(a+b, sum, '断言失败， %s + %s != %s' % (a, b, sum))
        except AssertionError, e:
            print e

    def test_assertNotEqual(self):
        try:
            a, b = 5, 2
            sub = 3
            self.assertNotEqual(a-b, sub, '断言失败， %s - %s != %s' % (a, b, sub))
        except AssertionError, e:
            print e

    def test_assertTrue(self):
        try:
            self.assertTrue(1 == 1, '1==1表达式为假')
        except AssertionError, e:
            print e

    def test_assertFalse(self):
        try:
            self.assertFalse(3 == 2, '3==2表达式为真')
        except AssertionError, e:
            print e

    def test_assertIs(self):
        try:
            a = 12
            b = a
            self.assertIs(a, b, '%s 与 %s 不属于同一对象' % (a, b))
        except AssertionError, e:
            print e


    def test_assertIsNot(self):
        try:
            a = 12
            b = 'test'
            self.assertIsNot(a, b, '%s 与 %s 属于同一对象' % (a, b))
        except AssertionError, e:
            print e


    def test_assertIsNone(self):
        try:
            a = myClass.return_None()
            self.assertIsNone(a,  '%s not is None' % (a))
        except AssertionError, e:
            print e

    def test_assertIsNotNone(self):
        try:
            a = myClass.sum(3, 2)
            self.assertIsNotNone(a,  '%s is None' % (a))
        except AssertionError, e:
            print e

    def test_assertIn(self):
        try:
            a = 'this is a test'
            b = 'test'
            self.assertIn(b, a, '%s 不包含在 %s 中' % (a, b))
        except AssertionError, e:
            print e

    def test_assertNotIn(self):
        try:
            a = 'this is a test'
            b = 'selenium'
            self.assertNotIn(b, a, '%s 包含在 %s 中' % (a, b))
        except AssertionError, e:
            print e


    def test_assertIsInstance(self):
        try:
            a = myClass
            b = object
            self.assertIsInstance(a, b, '%s 的类型不是 %s ' % (a, b))
        except AssertionError, e:
            print e

    def test_assertNotIsInstance(self):
        try:
            a = 123
            b = str
            self.assertNotIsInstance(a, b, '%s 的类型是 %s ' % (a, b))
        except AssertionError, e:
            print e

    def test_assertRaises(self):
        with self.assertRaises(ValueError) as cm:
            random.sample([1,2,3,4,5], 'j')
        print '===', cm.exception

        try:
            self.assertRaises(ZeroDivisionError, myClass.div, 3, 0)
        except ZeroDivisionError, e:
            print e


    def test_assertRaisesRegexp(self):
        with self.assertRaisesRegexp(ValueError, 'literal') as ar:
            int('xyz')
        print ar.exception
        print ar.expected_regexp
        try:
            self.assertRaisesRegexp(ValueError,
                 "invalid literal for. *XYZ'$", int, 'XYZ')
        except AssertionError, e:
            print e


if __name__ == '__main__':
    example_tests = unittest.TestLoader().loadTestsFromTestCase(myAssert)

    suite = unittest.TestSuite([example_tests])

    runner = HtmlTestRunner.HTMLTestRunner()

    runner.run(suite)
