#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  test_get.py [Python]
# @Version    :  
# @Author     :  mitool
# @Date       :  2019/7/15
# @IDE        :  PyCharm
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------

from selenium import webdriver
from selenium.webdriver.common.by import By
import unittest
import time

class web(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print '>>>>>>>>>>'
        # self.driver = webdriver.Ie(executable_path="C:\webdriver\IEDriverServer")
        # self.driver = webdriver.Firefox(executable_path="C:\webdriver\geckodriver")
        cls.driver = webdriver.Chrome(executable_path="C:\webdriver\chromedriver.exe")
        cls.driver.maximize_window()


    @unittest.skip("skipping")
    def test_get(self):
        self.driver.get("http://www.sogou.com")
        assert self.driver.title.find(u'搜狗搜索引擎')>=0, 'assert Error'

    @unittest.skip('2')
    def test_back_forward(self):
        first = "http://www.sogou.com/"
        second = "https://www.baidu.com/"
        self.driver.get(first)
        self.driver.get(second)
        self.driver.back()
        self.driver.forward()
        assert self.driver.current_url == second

    @unittest.skip('3')
    def test_refresh(self):
        self.driver.get("http://www.sogou.com")
        self.driver.refresh()

    @unittest.skip('4')
    def test_maxsize(self):
        self.driver.maximize_window()
        self.driver.get("http://www.sogou.com")

    @unittest.skip("5")
    def test_window_position(self):
        url = "https://www.baidu.com/"
        self.driver.get(url)
        position = self.driver.get_window_position()
        print "当前浏览器所在位置的坐标(x,y)={}".format(position)
        time.sleep(2)
        self.driver.set_window_position(y=20, x=40)
        print self.driver.get_window_position()

    @unittest.skip("6")
    def test_window_size(self):
        url = "https://www.baidu.com/"
        self.driver.get(url)
        size = self.driver.get_window_size()
        print '当前浏览器窗口：{}'.format(size)
        time.sleep(1)
        self.driver.set_window_size(200, 400, 'current')
        print self.driver.get_window_size('current')

    @unittest.skip("7")
    def test_get_title(self):
        url = "https://www.baidu.com/"
        self.driver.get(url)
        title = self.driver.title
        print title
        assert title == u"百度一下，你就知道"

    @unittest.skip('8')
    def test_get_page_source(self):
        url = "http://www.sogou.com"
        self.driver.get(url)
        page = self.driver.page_source
        assert u"新闻" in page

    @unittest.skip('9')
    def test_get_current_url(self):
        url = "http://www.sogou.com"
        self.driver.get(url)
        current = self.driver.current_url
        assert current == 'https://www.sogou.com/'
        self.assertEqual(current, 'https://www.sogou.com/', '当前网页网址非预期！')

    def test_operate_window_handle(self):
        url = 'https://www.baidu.com'
        self.driver.get(url)
        now_handle = self.driver.current_window_handle
        print now_handle

        self.driver.find_element(By.ID, 'kw').send_keys('w3cschool')
        self.driver.find_element(By.ID, 'su').click()
        time.sleep(3)

        self.driver.find_element(By.XPATH, '//*[@id="1"]/h3/a[1]').click()
        time.sleep(5)

        all_handle = self.driver.window_handles
        print '+++++', self.driver.window_handles[-1]

        for handle in all_handle:
            if handle != now_handle:
                self.driver.switch_to.window(handle)
        self.driver.find_element(By.LINK_TEXT, 'HTML5').click()
        time.sleep(3)
        self.driver.close()
        time.sleep(3)

        print now_handle
        self.driver.switch_to.window(now_handle)
        time.sleep(2)

        self.driver.find_element(By.ID, 'kw').clear()
        self.driver.find_element(By.ID, 'kw').send_keys(u'光荣之路自动化测试培训')
        self.driver.find_element(By.ID, 'su').click()
        time.sleep(5)

    @classmethod
    def tearDownClass(cls):
        print "<<<<<<<<<<<<"
        time.sleep(1)
        cls.driver.close()
        cls.driver.quit()


if __name__ == '__main__':
    unittest.main()