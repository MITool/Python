#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  test_feature.py [Python]
# @Version    :  
# @Author     :  mitool
# @Date       :  2019/7/14
# @IDE        :  PyCharm
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------

import pytest
import allure

@allure.feature('test_module_01')
@allure.story('test_story_01')
def test_case_01():
    """
       用例描述：Test case 01
       """
    assert 0


@allure.feature('test_module_02')
@allure.story('test_story_02')
def test_case_02():
    """
    用例描述：Test case 02
    """
    assert 0 == 0


if __name__ == '__main__':
    pytest.main(['-s', '-q', '--alluredir', './report/result'])

