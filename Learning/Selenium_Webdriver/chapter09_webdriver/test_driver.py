#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  test_driver.py [Python]
# @Version    :  
# @Author     :  mitool
# @Date       :  2019/7/15
# @IDE        :  PyCharm
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------

from selenium import webdriver
import unittest

class ByDriver(unittest.TestCase):

    def setUp(self):
        print '>>>>>>>>>>'
        # self.driver = webdriver.Ie(executable_path="C:\webdriver\IEDriverServer")
        # self.driver = webdriver.Firefox(executable_path="C:\webdriver\geckodriver")
        self.driver = webdriver.Chrome(executable_path="C:\webdriver\chromedriver.exe")

    def test_get_sogou(self):
        self.driver.get("http://www.sogou.com")
        print self.driver.current_url

    def tearDown(self):
        print "<<<<<<<<<<<<"
        self.driver.close()
        self.driver.quit()

if __name__ == '__main__':
    unittest.main()
