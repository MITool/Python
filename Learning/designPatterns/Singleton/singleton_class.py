#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  singleton_class.py [Python]
# @Version    :  
# @Author     :  mitool
# @Date       :  2019/6/30
# @IDE        :  PyCharm
# ---------------------------------------------------
'''
    Description:
         使用类实现单例模式
'''
# ---------------------------------------------------

import time
import threading


class singleton(object):

    def __init__(self):
        time.sleep(1)
        pass


    @classmethod
    def instance(cls, *args, **kwargs):
        if not hasattr(singleton, '_instance'):
            singleton._instance = singleton(*args, **kwargs)
        return singleton._instance


#   这样当多线程时，存在问题


def task(arg):
    obj = singleton.instance()
    print(obj)


for i in range(10):
    t = threading.Thread(target=task, args=[i, ])
    t.start()

'''
    看起来也没有问题，那是因为执行速度过快，如果在init方法中有一些IO操作，就会发现问题了，下面我们通过time.sleep模拟

    我们在上面__init__方法中加入以下代码：

    def __init__(self):
        import time
        time.sleep(1)
        
    无法支持多线程。
    
    解决办法：加锁，未加锁部分并发执行，加锁部分串行执行，速度降低，但保证了数据安全
'''



class Singleton(object):

    _instance_lock = threading.Lock()

    def __init__(self):
        time.sleep(2)

    @classmethod
    def instance(cls, *args, **kwargs):
        if not hasattr(Singleton, '_instance'):
            with Singleton._instance_lock:
                if not hasattr(Singleton, '_instance'):
                    Singleton._instance = Singleton(*args, **kwargs)
        return Singleton._instance

def task_1(arg):
    obj = Singleton.instance()
    print(obj)


for i in range(10):
    t = threading.Thread(target=task_1,args=[i,])
    t.start()

time.sleep(20)
obj = Singleton.instance()
print(obj)


'''
    这种方式实现的单例模式，使用时会有限制，以后实例化必须通过 obj = Singleton.instance() 

    如果用 obj=Singleton() ,这种方式得到的不是单例
'''
