#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  singleton_decorator.py [Python]
# @Version    :  
# @Author     :  mitool
# @Date       :  2019/6/30
# @IDE        :  PyCharm
# ---------------------------------------------------
'''
    Description:

'''
# ---------------------------------------------------

'''

    1、使用模块

    其实，Python 的模块就是天然的单例模式，因为模块在第一次导入时，会生成 .pyc 文件，
    当第二次导入时，就会直接加载 .pyc 文件，而不会再次执行模块代码。因此，我们只需把
    相关的函数和数据定义在一个模块中，就可以获得一个单例对象了。
    如果我们真的想要一个单例类，可以考虑这样做：

    mysingleton.py

    class Singleton(object):
        def foo(self):
            pass
    singleton = Singleton()
    将上面的代码保存在文件 mysingleton.py 中，要使用时，直接在其他文件中导入此文件中的对象，这个对象即是单例模式的对象

    from a import singleton

'''


 #   2、使用装饰器

def singleton(cls):
    _instance = {}

    def _singleton(*args, **kwargs):
        if cls not in _instance:
            _instance[cls] = cls(*args, **kwargs)
        return _instance[cls]
    return _singleton

@singleton
class A(object):
    a = 1

    def __init__(self, x=0):
        self.x = x


if __name__ == '__main__':

    # 2、装饰器
    a1 = A(2)
    a2 = A(3)
    print(a1)
    print(a2)
