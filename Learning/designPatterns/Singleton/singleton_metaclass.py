#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  singleton_metaclass.py [Python]
# @Version    :  
# @Author     :  mitool
# @Date       :  2019/6/30
# @IDE        :  PyCharm
# ---------------------------------------------------
'''
    Description:
         基于 metaclass 实现
'''
# ---------------------------------------------------

'''
    1、类由type创建。创建类时，type的__init__方法自动执行，
       类() 执行type的__call__方法（类的__new__方法，__init__方法）
    2、对象由类创建。创建对象时，类的__init__方法自动执行，
       对象() 执行类的 __call__ 方法
'''

class Foo:

    def __init__(self):
        pass

    def __call__(self, *args, **kwargs):
        pass

obj = Foo()
obj()
'''
    1、类Foo 是 type的对象；
    2、Foo() 执行 type 的__call__方法，调用Foo类的__new__方法，创建对象，
       调用Foo类的__init__方法，对对象初始化
    3、obj 是类Foo实例化对象；
    4、obj() 执行Foo的__call__方法
'''


class singletonType(type):

    def __init__(self, *args, **kwargs):
        super(singletonType, self).__init__(*args, **kwargs)


    def __call__(cls, *args, **kwargs): # 这里的cls, 即Foo类
        print('cls : ', cls)
        obj = cls.__new__(cls, *args, **kwargs)
        cls.__init__(obj, *args, **kwargs) # Foo.__init__(obj)
        return obj


class Foo(metaclass=singletonType): # 指定创建Foo的type为singletonType
    def __init__(self, name):
        self.name = name

    def __new__(cls, *args, **kwargs):
        return object.__new__(cls)

obj = Foo('xx')


import threading

class SingletonType(type):
    _instance_lock = threading.Lock()

    def __call__(cls, *args, **kwargs):
        if not hasattr(cls, "_instance"):
            with SingletonType._instance_lock:
                if not hasattr(cls, "_instance"):
                    cls._instance = super(SingletonType, cls).__call__(*args, **kwargs)
        return cls._instance

class Foo(metaclass=SingletonType):
    def __init__(self,name):
        self.name = name


obj1 = Foo('name1')
obj2 = Foo('name2')
print(obj1, obj2)

