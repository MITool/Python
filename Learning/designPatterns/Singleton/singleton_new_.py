#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  singleton_new_.py [Python]
# @Version    :  
# @Author     :  mitool
# @Date       :  2019/6/30
# @IDE        :  PyCharm
# ---------------------------------------------------
'''
    Description:
         基于__new__方法实现 单例模式, 采用这种方式的单例模式，
         以后实例化对象时，和平时实例化对象的方法一样 obj = Singleton()
'''
# ---------------------------------------------------

'''
    当我们实例化一个对象时， 是先执行了类的__new__方法（我们没重写时，默认调用object.__new__）实例化对象，
    然后再执行类的__init__方法，对这个对象进行初始化
'''

import time
import threading

class Singleton(object):

    _instance_lock = threading.Lock()

    def __init__(self):
        # time.sleep(2)
        pass

    def __new__(cls, *args, **kwargs):
        if not hasattr(Singleton, '_instance'):
            with Singleton._instance_lock :
                if not hasattr(Singleton, '_instance'):
                    Singleton._instance = object.__new__(cls)
        return Singleton._instance


obj1 = Singleton()
obj2 = Singleton()
print(obj1, '\n', obj2)

def task(arg):
    obj = Singleton()
    print(obj)

for i in range(10):
    t = threading.Thread(target=task,args=[i,])
    t.start()


'''
#encoding=utf8
import threading
import time
#这里使用方法__new__来实现单例模式
class Singleton(object):#抽象单例
    def __new__(cls, *args, **kw):
        if not hasattr(cls, '_instance'):
            orig = super(Singleton, cls)
            cls._instance = orig.__new__(cls, *args, **kw)
        return cls._instance
#总线
class Bus(Singleton):
    lock = threading.RLock()
    def sendData(self,data):
        self.lock.acquire()
        time.sleep(3)
        print "Sending Signal Data...",data
        self.lock.release()
#线程对象，为更加说明单例的含义，这里将Bus对象实例化写在了run里
class VisitEntity(threading.Thread):
    my_bus=""
    name=""
    def getName(self):
        return self.name
    def setName(self, name):
        self.name=name
    def run(self):
        self.my_bus=Bus()
        self.my_bus.sendData(self.name)

if  __name__=="__main__":
    for i in range(3):
        print "Entity %d begin to run..."%i
        my_entity=VisitEntity()
        my_entity.setName("Entity_"+str(i))
        my_entity.start()

'''

