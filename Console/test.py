#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  thirdParties.py [Python]
# @Version    :  python3.6
# @Author     :  mitool
# @Date       :  2018/11/5
# @IDE        :  PyCharm

'''
    Description:
         
'''
# ---------------------------------------------------

import os
import sys
#
# BASE_DIR = os.path.dirname(os.path.abspath(__file__))
# LOG_DIR = os.path.join(BASE_DIR + '/../', "logs")
#
# if not os.path.exists(LOG_DIR):
#     os.makedirs(LOG_DIR)

from optparse import OptionParser

import execjs
print(execjs.eval("new Date"))
# 返回值为： 2018-04-04T12:53:17.759Z
# print execjs.eval("Date.now()")
# 返回值为：1522847001080  # 需要注意的是返回值是13位， 区别于python的time.time()

ctx = execjs.compile(''' function test () {
        var s = [];
        var hexDigits = "0123456789abcdef";
        for (var i = 0; i < 36; i++) {
            s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
        }
        s[14] = "4"; // bits 12-15 of the time_hi_and_version field to 0010
        s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1); // bits 6-7 of the clock_seq_hi_and_reserved to 01
        s[8] = s[13] = s[18] = s[23] = "-";
        var uuid = s.join("");
        return uuid;
    }''')

# 此处的'thirdParties'为js的函数名称
print(ctx.call('test'))

print(os.path.abspath('.'))
print(os.getcwd())
print(sys.platform)
print(os.path.join(os.path.expanduser("~"), ".wdm"))


class Fib(object):
    def __init__(self):
        pass

    def __call__(self, num):
        a, b = 0, 1
        self.l = []

        for i in range(num):
            self.l.append(a)
            a, b = b, a + b
        return self.l

    def __str__(self):
        return str(self.l)

    __rept__ = __str__


f = Fib()
print(f(10))



