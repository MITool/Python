#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  test1.py [Python]
# @Version    :  PY2
# @Author     :  mitool
# @Date       :  2019/4/14
# @IDE        :  PyCharm
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------
# import filetype
# import tarfile
#
# kind = filetype.guess("C:\\Users\\mitool\\Downloads\\geckodriver-v0.24.0-macos.tar.gz")
# print(kind.extension)
#
# print(tarfile.is_tarfile("C:\\Users\\mitool\\Downloads\\geckodriver-v0.24.0-macos.tar.gz"))

# import requests
# resp = requests.get("https://www.baidu.com")
# if resp.status_code == 200:
#     print(requests.utils.default_user_agent())

# import os
#
# env_dist = os.environ # environ是在os.py中定义的一个dict environ = {}
#
# print(env_dist.get('JAVA_HOME'))
# print(env_dist['JAVA_HOME'])
#
# # 打印所有环境变量，遍历字典
# for key in env_dist:
#     print(key + ' : ' + env_dist[key])


# string = 'javareewfjavawooeewjavaerjeoirejava'
#
# print(string.count('java'))
# print(string.count('e'))
#
# def subcount(string, substr=None):
#     count = 0
#     for i in range(0, len(string)):
#         tmp = len(substr)+i
#         if( tmp<= len(string) and string[i:tmp] == substr):
#             count += 1
#     print(count)
#
# subcount(string, 'java')
# subcount(string, 'e')

a0 = dict(zip(('a','b', 'c', 'd', 'e'), (1, 2, 3, 4, 5)))
# a1 = range(10)
# a2 = [i for i in a1 if i in a0]
#
#
# print(a0)
# print(a1)
# print(a2)


import json
import sys

params = "{\"sJson\":\"{\\\"operationtype\\\":1,\\\"content\\\":{\\\"senderid\\\":438,\\\"sendername\\\":\\\"许峰\\\",\\\"receiverid\\\":\\\"220\\\",\\\"receivername\\\":\\\"邓强 \\\",\\\"mtitle\\\":\\\"test\\\",\\\"mcontent\\\":\\\"test\\\",\\\"attlist\\\":[]}}\"}"



print(json.dumps(json.dumps(a0)))








