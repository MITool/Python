#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  webdriverDemo.py [Python]
# @Version    :  python3.6
# @Author     :  mitool
# @Date       :  2018/9/3
# @IDE        :  PyCharm
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------

from selenium import webdriver
from selenium.webdriver.common.by import By

import os
import time

ROOTPATH = os.path.dirname(os.path.realpath(__file__))
print(ROOTPATH)

path = os.path.join(ROOTPATH, "packages", "chromedriver.exe")
print(path)

login_path = (By.XPATH, '//div[@class="sn-container"]//a[@class="sn-login"]')
pwd_login =(By.XPATH, '//*[@id="login"]//*[contains(text(),"密码登录")]')
sms_login =(By.XPATH, '//*[@id="login"]//*[contains(text(),"短信登录")]')

user_path = (By.XPATH, '//*[@id="login"]//input[@name="fm-login-id"]')
pwd_path = (By.XPATH, '//*[@id="login"]//input[@name="fm-login-password"]')
submit_btn =(By.XPATH, '//div[@id="login"]//div[@class="fm-btn"]')
def sendKeys(driver, xpath, input):
    driver.find_element(*xpath).clear()
    driver.find_element(*xpath).send_keys(input)

def click(driver, xpath:tuple):
    driver.find_element(*xpath).click()


driver = webdriver.Chrome(executable_path=path)
# driver = getattr(webdriver, 'Chrome')
# dri = driver()
# print(dri.capabilities['version'])

url = "https://www.tmall.com/"

driver.get(url)
driver.set_window_size(1920, 1680)
driver.implicitly_wait(2)
print(driver.capabilities)

# driver.find_element(By.XPATH, '//div[@class="sn-container"]//a[@class="sn-login"]').click()
click(driver, login_path)
driver.switch_to.frame("J_loginIframe")
# driver.find_element(By.XPATH, '//*[@id="login"]//*[contains(text(),"短信登录")]').click()
# driver.find_element(By.XPATH, '//*[@id="login"]//input[@name="fm-login-id"]').clear()
sendKeys(driver, user_path, "13800138000")
sendKeys(driver, pwd_path, "12345678")
click(driver, submit_btn)

time.sleep(5)


driver.quit()

# driver.close()
os.system('taskkill /im chromedriver.exe /F')



