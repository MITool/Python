#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  Dict.py [Python]
# @Version    :  PY2
# @Author     :  mitool
# @Date       :  2019/4/13
# @IDE        :  PyCharm
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------

class Dict(dict):
    """ custom dict."""

    def __getattr__(self, key):
        return self.get(key, None)

    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__

    # for section in self.cf.sections():
    #     setattr(self, section, Dict())
    #     for option, value in self.cf.items(section):
    #         try:
    #             # Ugly fix to avoid '0' and '1' to be parsed as a boolean value.
    #             # We raise an exception to goto fail^w parse it as integer.
    #             if self.cf.get(section, option) in ["0", "1"]:
    #                 raise ValueError
    #             value = self.cf.getboolean(section, option)
    #         exception ValueError:
    #             try:
    #                 value = self.cf.getint(section, option)
    #             exception ValueError:
    #                 value = self.cf.get(section, option)
    #
    #         setattr(getattr(self, section), option, value)

    # def get(self, section):
    #     """
    #     get option
    #     :param section: section to fetch.
    #     :return: option value.
    #     """
    #     try:
    #         return getattr(self, section)
    #     exception AttributeError as e:
    #         raise Error("Section [%s] is not found in configuration, error: %s" % (section, e))