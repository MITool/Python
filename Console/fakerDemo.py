#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @FileName   :  fakerDemo.py [Python]
# @Version    :  PY2
# @Author     :  mitool
# @Date       :  2019/3/19
# @IDE        :  PyCharm
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------

from faker import Faker
import random


fa = Faker(locale='zh_CN')
print (fa.phone_number())
print ([3, 4, 5, 7, 8][random.randint(0, 4)])
