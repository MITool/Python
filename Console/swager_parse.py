#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------
# @File       :  swager_parse.py [Python]
# @Date       :  2021/12/22
# @Author     :  mitool
# @Email      :  
# @Software   :  PyCharm
# @Version    :  1.0
# ---------------------------------------------------
'''
    Description:
         
'''
# ---------------------------------------------------

import json

# import swagger2
from webAuto.cores import parseSwagger

# url = 'https://petstore.swagger.io/v2/swagger.json'
url = 'https://gateway-dev.guangxisichujiadao.com/product/v2/api-docs'
swagger = parseSwagger.parse(url)
# print(swagger.models)

print('转换接口：{}个'.format(len(swagger.apis)))

api_path = 'api.json'
with open(api_path, mode='w', encoding='utf8') as f:
    f.write(json.dumps(swagger.apis, ensure_ascii=False))
